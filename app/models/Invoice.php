<?php

class Invoice extends Eloquent
{

    protected $table = 'invoice';
    protected $fillable = [];

    public function ownerstall()
    {
        return $this->belongsTo('OwnerStall');
    }
    
    public function getMonthAttribute($value)
    {
        $datames = date("m", strtotime($value));
        switch ($datames) {
            case '01':
                $mes = 'Gener';
                break;
            case '02':
                $mes = 'Febrer';
                break;
            case '03':
                $mes = 'Març';
                break;
            case '04':
                $mes = 'Abril';
                break;
            case '05':
                $mes = 'Maig';
                break;
            case '06':
                $mes = 'Juny';
                break;
            case '07':
                $mes = 'Juliol';
                break;
            case '08':
                $mes = 'Agost';
                break;
            case '09':
                $mes = 'Setembre';
                break;
            case '10':
                $mes = 'Octubre';
                break;
            case '11':
                $mes = 'Novembre';
                break;
            case '12':
                $mes = 'Desembre';
                break;
            default:
                $mes = '';
                break;
        }
        return $mes . ' ' . date("Y", strtotime($value));
    }
    
    public function getPaidAttribute($value)
    {
        $datames = date("m", strtotime($value));
        switch ($datames) {
            case '01':
                $mes = 'de gener';
                break;
            case '02':
                $mes = 'de febrer';
                break;
            case '03':
                $mes = 'de març';
                break;
            case '04':
                $mes = 'd\'Abril';
                break;
            case '05':
                $mes = 'de maig';
                break;
            case '06':
                $mes = 'de juny';
                break;
            case '07':
                $mes = 'de juliol';
                break;
            case '08':
                $mes = 'd\'agost';
                break;
            case '09':
                $mes = 'de setembre';
                break;
            case '10':
                $mes = 'd\'octubre';
                break;
            case '11':
                $mes = 'de novembre';
                break;
            case '12':
                $mes = 'de desembre';
                break;
            default:
                $mes = '';
                break;
        }
        return is_null($value) ? NULL : date("d", strtotime($value)) . ' ' . $mes . ' ' . date("Y", strtotime($value));
    }
}

<?php

class Sector extends Eloquent
{

    protected $table = 'sector';
    protected $fillable = array('name');


    public function authprods()
    {
        return $this->hasMany('AuthProd');
    }
}

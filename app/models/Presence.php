<?php

class Presence extends Eloquent
{

    protected $table = 'presence';
    protected $fillable = array('ownerstall_id', 'calendar_id');


    public function ownerstall()
    {
        return $this->belongsTo('OwnerStall');
    }
    public function calendar()
    {
        return $this->belongsTo('Calendar');
    }
}

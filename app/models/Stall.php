<?php

class Stall extends Eloquent
{

    protected $table = 'stall';
    protected $fillable = array('num', 'lenght');

    public function market()
    {
        return $this->belongsTo('Market');
    }
    
    public function sector()
    {
        return $this->belongsTo('Sector');
    }
    
    public function authprod()
    {
        return $this->belongsTo('AuthProd', 'auth_prod_id');
    }
    
    public function owners()
    {
        return $this->belongsToMany('Owner', 'ownerstall')
            ->withPivot('id', 'insurance', 'discharge_date', 'expire_date', 'deactivated_at', 'created_at')
            ->orderBy('deactivated_at', 'asc');
    }
    
    public function activeOwner()
    {
        return $this->belongsToMany('Owner', 'ownerstall')
            ->withPivot('id', 'insurance', 'discharge_date', 'expire_date', 'deactivated_at', 'created_at')
            ->where('deactivated_at', '=', null);
    }
    
    public function inactiveOwners()
    {
        return $this->belongsToMany('Owner', 'ownerstall')
            ->withPivot('id', 'insurance', 'discharge_date', 'expire_date', 'deactivated_at', 'created_at')
            ->where('deactivated_at', '!=', null);
    }


    public function absences()
    {
        return $this->HasMany('Absence', 'ownerstall_id', 'num');
    }
}

<?php

class Market extends Eloquent
{

    protected $table = 'market';
    protected $fillable = array('name', 'dayofweek', 'town_id');

    public function town()
    {
        return $this->belongsTo('Town');
    }
    
    public function stalls()
    {
        return $this->hasMany('Stall');
    }

}

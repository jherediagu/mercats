<?php

class OwnerStall extends Eloquent
{

    protected $table = 'ownerstall';
    protected $fillable = array('owner_id', 'stall_id', 'discharge_date', 'expire_date', 'insurance', 'tariff_id');

    public function owner()
    {
        return $this->hasOne('Owner', 'id', 'owner_id');
    }
    
    public function stall()
    {
        return $this->hasOne('Stall', 'id', 'stall_id');
    }
    
    public function tariff()
    {
        return $this->belongsTo('Tariff');
    }
    
//    public function getDischargeDateAttribute($value)
//    {
//        return strftime("%A %d de %b de %Y", strtotime($value));
//    }
//    
//    public function getExpireDateAttribute($value)
//    {
//        return strftime("%A %d de %b de %Y", strtotime($value));
//    }
    
    public function presence()
    {
        return $this->belongsToMany('Calendar', 'presence', 'ownerstall_id');
    }
    
    public function notes()
    {
        return $this->belongsToMany('Calendar', 'notes', 'ownerstall_id')
                ->withPivot('happens_at', 'title', 'description', 'sent_to', 'solved_at');
    }
}

<?php

class Town extends Eloquent
{

    protected $table = 'town';
    protected $fillable = array('name');

    public function markets()
    {
        return $this->hasMany('Market', 'town_id', 'id');
    }

}

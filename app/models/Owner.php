<?php

class Owner extends Eloquent
{

    protected $table = 'owner';
    protected $fillable = array('name', 'address', 'email', 'dni', 'phone', 'iban', 'sname', 'saddress', 'semail', 'sdni', 'sphone');

    
    public function stalls()
    {
        return $this->belongsToMany('Stall', 'ownerstall')
            ->withPivot('insurance', 'discharge_date', 'expire_date', 'deactivated_at', 'created_at')
            ->where('market_id', '=', Session::get('market'));
    }
    
    public function activeStalls()
    {
        return $this->belongsToMany('Stall', 'ownerstall')
            ->withPivot('insurance', 'discharge_date', 'expire_date', 'deactivated_at', 'created_at')
            ->where('market_id', '=', Session::get('market'))
            ->whereNull('deactivated_at');
    }
    
}

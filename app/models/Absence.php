<?php

class Absence extends Eloquent
{

    protected $table = 'absence';
    protected $fillable = array('ownerstall_id', 'start_at', 'end_at', 'document');


    public function setStartAtAttribute($value)
    {
        $this->attributes['start_at'] = date('Y-m-d', strtotime($value));
    }
    
    public function setEndAtAttribute($value)
    {
        $this->attributes['end_at'] = date('Y-m-d', strtotime($value));
    }
    
    public function ownerstall()
    {
        return $this->belongsTo('OwnerStall');
    }
}

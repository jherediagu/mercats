<?php

class Calendar extends Eloquent
{

    protected $table = 'calendar';
    protected $fillable = array('date');

    public function getDateAttribute($value)
    {
        return date("d-m-Y", strtotime($value));
    }
    
        
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date('Y-m-d', strtotime($value));
    }
    
//    public function setDateAttribute($value)
//    {
//        $this->attributes['date'] = date('Y-m-d', strtotime($value));
//    }
}

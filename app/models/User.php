<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait,
        RemindableTrait;

    protected $table = 'users';
    protected $hidden = array('password');
    protected $fillable = array('name', 'surname', 'email', 'dni', 'role','remember_token');

    public function markets()
    {
        return $this->belongsToMany('Market', 'user_market', 'user_id', 'market_id');
    }

}

<?php

class Note extends Eloquent
{

    protected $table = 'notes';
    
    protected $fillable = array('title', 'description', 'sent_to', 'happens_at', 'solved_at');


    public function setHappensAtAttribute($value)
    {
        $this->attributes['happens_at'] = date('Y/m/d h:i:s', strtotime($value));
    }
    public function setSolvedAtAttribute($value)
    {
        $this->attributes['solved_at'] = is_null($value) ? NULL : date('Y/m/d h:i:s', strtotime($value));
    }
    
    public function ownerstall()
    {
        return $this->belongsTo('OwnerStall');
    }
//    
//    public function getDates()
//    {
//        return ['happens_at', 'solved_at'];
//    }
}

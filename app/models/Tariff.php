<?php
class Tariff extends Eloquent
{

    protected $table = 'tariff';

    protected $fillable = array('name', 'price');

}

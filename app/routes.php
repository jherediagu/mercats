<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Display all SQL executed in Eloquent 
/*Event::listen('illuminate.query', function($query)
{
    var_dump($query);
});*/

// canviar pass admin
Route::get('crearpass_admin', function()
{
    DB::table('users')
            ->where('id', 10)
            ->update(array('password' => Hash::make('UFdn24XTY')));
});



Route::get('login', 'AuthController@getLogIn');

Route::post('login', 'AuthController@postLogIn');

Route::controller('password', 'RemindersController');


//Invoice test (Rebuts)
Route::post('print/invoice/print', ['as' => 'invoice.payprintapp', 'uses' => 'InvoiceController@payprintapp']);
Route::get('print/invoice/print', ['as' => 'invoice.payprintapp', 'uses' => 'InvoiceController@payprintapp']);



Route::group(array('before' => 'auth'), function()
{
    Route::get('error', function(){
        return View::make('private.modelexception'); 
    });



    Route::get('/', 'HomeController@home');
    
    Route::get('logout', 'AuthController@postLogOut');

    Route::get('move/{id}', 'AuthController@move');

    Route::get('stall/getinfo', 'StallController@getInfo');

    Route::get('stall/ajaxSearch', 'StallController@ajaxSearch');

    Route::any('mapa', 'MapController@showMap');

    Route::post('presence/toggle', 'PresenceController@togglePresence');

    Route::post('sector/editProducts/{id}', 'SectorController@editProducts');
    Route::post('sector/deleteProducts/{id}', 'SectorController@deleteProducts');


    
    //ADMIN
    Route::group(array('before' => 'admin'), function()
    {
        Route::resource('user', 'UserController');
        Route::get('stall/{id}', 'StallController@show');
        Route::get('stall/{id}/edit', 'StallController@editStall');


        

    });
    
    // INT
    Route::group(array('before' => 'int'), function()
    {
        Route::get('ownerstall/ajaxStallSearch', 'OwnerStallController@ajaxStallSearch');
        Route::get('ownerstall/ajaxOwnerSearch', 'OwnerStallController@ajaxOwnerSearch');
        Route::resource('ownerstall', 'OwnerStallController');

        Route::resource('sector', 'SectorController');

        Route::resource('authprod', 'AuthProdController');

        Route::resource('calendar', 'CalendarController');

        Route::resource('absence', 'AbsenceController');

        Route::resource('tariff', 'TariffController');
    });
    
    // TAB
    Route::group(array('before' => 'tab'), function()
    {
        // OWNER
        Route::get('owner/ajaxSearch', 'OwnerController@ajaxSearch');
        Route::resource('owner', 'OwnerController');
        
        // STALL
/*        Route::get('stall/ajaxSearch', 'StallController@ajaxSearch2');
*/        Route::resource('stall', 'StallController');
        
        // INVOICE
        Route::get('invoice/printout/{id}', ['as' => 'invoice.printout', 'uses' => 'InvoiceController@printout']);
        Route::get('invoice/payprint/{id}', ['as' => 'invoice.payprint', 'uses' => 'InvoiceController@payprint']);
        Route::resource('invoice', 'InvoiceController');
        Route::get('invoice/printandpay/{id}',['as' => 'invoice.printandpay', 'uses' => 'InvoiceController@printandpay']);
        Route::get('notpaid', ['as' => 'invoice.notpaid', 'uses' => 'InvoiceController@indexNotPaid']);

        // PRESENCE
        Route::resource('presence', 'PresenceController');
        Route::get('presence_no',   'PresenceController@indexPresence');

        // NOTES
        Route::resource('notes', 'NotesController');

        // AUTHPROD
        Route::get('authprod/sector/{id}',['as' => 'authprod.showsector', 'uses' => 'AuthProdController@relation_sector_products']);

    });

    
    
});
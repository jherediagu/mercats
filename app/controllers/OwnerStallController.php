<?php

class OwnerStallController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $ownerstalls = OwnerStall::whereHas('stall', function($q){
                            $q->where('market_id', '=', Session::get('market'));
                        })
                        ->with('stall', 'stall.authprod', 'stall.sector', 'owner', 'tariff')
                        ->get();
                        
        return View::make('private.ownerstall.index')
                    ->with('ownerstalls', $ownerstalls);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $owner = Owner::find(Input::get('owner_id'));
        $stall = Stall::find(Input::get('stall_id'));
        $tariff = Tariff::where('market_id', Session::get('market'))->lists('name', 'id');
        
        return View::make('private.ownerstall.create')
                ->with('owner', $owner)
                ->with('stall', $stall)
                ->with('tariff', $tariff);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $nice = ['owner_id' => 'titular',
                'stall_id' => 'parada',
                'tariff_id' => 'tarifa',
                'insurance' => 'assegurança',
                'discharge_date' => 'data d\'alta',
                'expire_date' => 'data de caducitat'];
        
        $rules = [  'owner_id' => 'required|exists:owner,id', //check if is authorized!
                    'stall_id' => 'required|exists:stall,id', //check if is authorized!
                    'tariff_id' => 'required|exists:tariff,id',
                    'insurance' => 'mimes:pdf', 
                    'discharge_date' => 'required|date',
                    'expire_date' => 'required|date'];
        
        $validation = Validator::make(Input::all(), $rules);
        $validation->setAttributeNames($nice);
        
        if ($validation->fails())
        {
            $params = [ 'owner_id' => Input::get('owner_id'),
                        'stall_id' => Input::get('stall_id') ];
            
            return Redirect::route('ownerstall.create', $params)
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            
            $ownerstall = new OwnerStall(Input::all());
            
            if (Input::hasFile('insurance') && Input::file('insurance')->isValid())
            {
                $file = Input::file('insurance');
                $ownerstall->insurance = strtolower('insurance-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("insurance", $ownerstall->insurance);
            }
            
            $ownerstall->save();

            return Redirect::route('ownerstall.index')
                            ->with(array('success_msg' => 'Titular assignat correctament.'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $sectors = DB::table('sector')->lists('name','id');

        return View::make('private.ownerstall.edit')
                        ->with('ownerstall', OwnerStall::findOrFail($id))
                        ->with('sectors',$sectors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $nice = ['discharge_date' => 'data d\'alta',
                'expire_date' => 'data de caducitat',
                'deactivated_at' => 'data de baixa'];
        
        $rules = [  'discharge_date' => 'required|date',
                    'expire_date' => 'required|date'];
        
        $validation = Validator::make(Input::all(), $rules);
        $validation->setAttributeNames($nice);
        
        if ($validation->fails())
        {
            
            return Redirect::route('ownerstall.edit')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {

            if (Input::get('deactivated_at') == '') 
            {
                $deactivated_at = NULL;
            }
            else
            {
                $deactivated_at = Input::get('deactivated_at');
            }

            DB::table('ownerstall')
                ->where('id', $id)
                ->update(
                    array(
                        'discharge_date'    => Input::get('discharge_date'),
                        'expire_date'       => Input::get('expire_date'),
                        'deactivated_at'    => $deactivated_at
                        )
                    );

            $ownerstall = OwnerStall::findOrFail($id);
            $ownerstall->update(Input::all());
        
            $ownerstall->save();

            return Redirect::route('ownerstall.index')
                            ->with(array('success_msg' => 'Dates editades correctament'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $deactivated_at = Input::get('deactivated_at');

        if ($deactivated_at == '') 
        {
            $deactivated_at = date('Y-m-d H:i:s');
        }
        
        $ownerstall = OwnerStall::findOrFail($id);
        $ownerstall->deactivated_at = $deactivated_at;
        
        if ($ownerstall->update())
            Session::flash('success_msg', 'Titular desactivat correctament.');
        else
            Session::flash('error_msg', 'No s\'ha pogut desactivar correctament.');
        
        return Redirect::to('ownerstall');
    }
    
    public function ajaxOwnerSearch()
    {
        $ownerstalls = OwnerStall::where('deactivated_at', '=', null)
                            ->whereHas('owner', function($q)
                            {
                                $q->where('name', 'like', '%'.Input::get('term').'%');
                            })
                            ->orWhereHas('owner', function($q)
                            {
                                $q->where('dni', 'like', '%'.Input::get('term').'%');
                            })
                            ->whereHas('stall', function($q)
                            {
                                $q->where('market_id', '=', Session::get('market'));
                            })
                            ->with('Owner')
                            ->with('Stall')
                            ->get();
        
        return Response::json($ownerstalls);
    }
    
    public function ajaxStallSearch()
    {
        $ownerstalls = OwnerStall::where('deactivated_at', '=', null)
                            ->whereHas('stall', function($q)
                            {
                                $q->where('num', 'like', '%'.Input::get('term').'%')
                                    ->where('market_id', '=', Session::get('market'));
                            })
                            ->with('Owner')
                            ->with('Stall')
                            ->get();
        
        return Response::json($ownerstalls);
    }

}

<?php

class MapController extends BaseController {

	
	/* Show map template from market id */
	public function showMap()
	{

		if(Session::has('market')){

			$has_date = Input::get('date');

			if ($has_date) {

				$date = $has_date;

			}else{

				$date = date('Y-m-d');

			} 


			if($day = Calendar::where('market_id', '=', Session::get('market'))->where('date', '=', $date. ' 00:00:00')->first()){
				$has_day = 1;
			}else{
				$day = new Calendar;
				$has_day = 0;
				$day->id = 0;
				//return Redirect::to('error');
			}


			$stalls = Stall::where('market_id', '=', Session::get('market'))
                ->with('authprod', 'sector', 'activeOwner')
                ->get();

            foreach ($stalls as $stall) {

            	if(isset($stall->active_owner[0]) && isset($stall->active_owner[0]->pivot) && isset($stall->active_owner[0]->pivot->id) && $has_day){

            		$stall->ownerstall_id = $stall->active_owner[0]->pivot->id;

            		if($presence = Presence::where('ownerstall_id', $stall->active_owner[0]->pivot->id)
	            					->where('calendar_id', $day->id)
	            					->first()){
            			$stall->has_presence = 1;
            			$stall->presence = $presence;
            		}else{
            			$stall->has_presence = 0;
            			$stall->presence = [];
            		}
				}else{
					$stall->has_presence = 0;
					$stall->presence = [];
					$stall->ownerstall_id = 0;

					// no te owner
					if (!isset($stall->active_owner[0])) {
						$stall->no_has_owner = 1;
					}
				}

				if ($absence = Absence::where('ownerstall_id', $stall->ownerstall_id)
										->where('start_at', '<=', $date." 00:00:00" )
										->where('end_at', '>=' , $date." 00:00:00" )
										->orderBy('created_at', 'desc')
										->first()) {
					$stall->has_absence = 1;
				}


            	$resposta['stalls'][] = $stall;
            }


			return View::make('private.maps.m' . Session::get('market'))
						->with('path_bg', 'imgs/maps/fons' . Session::get('market') . '.png')
						->with('base_url', URL::to('/'))
						->with('has_day', $has_day)
						->with('day', $day)
						->with('date', $date)
						->with('stalls', $resposta['stalls']);

		}else{
			return Redirect::route('error');
		}

	}


	/* GET MAIN INFO FOR MAP (recive id_market) */
	public function getInfo(){

		//Get today id_calendar
		if($day = Calendar::where('market_id', '=', Session::get('market'))->where('date', '=', date("Y-m-d") . ' 00:00:00')->first()){
			$resposta['day'] = $day;

			/*
			$presence = Presence::whereHas('calendar', function($q){
                        $q->where('market_id', Session::get('market'));
                    })
                    ->with('ownerstall', 'stall')
                    ->where('calendar_id', $day->id)
                    ->get();


            $resposta['presence'] = $presence;
           	*/
           	$stalls = Stall::where('market_id', '=', Session::get('market'))
                ->with('authprod', 'sector', 'activeOwner')
                ->get();

            foreach ($stalls as $stall) {

            	if(isset($stall->active_owner[0]) && isset($stall->active_owner[0]->pivot) && isset($stall->active_owner[0]->pivot->id)){
            		if($presence = Presence::where('ownerstall_id', $stall->active_owner[0]->pivot->id)
	            					->where('calendar_id', $day->id)
	            					->first()){
            			$stall->has_presence = 1;
            			$stall->presence = $presence;
            		}else{
            			$stall->has_presence = 0;
            			$stall->presence = [];
            		}
				}else{
					$stall->has_presence = 0;
					$stall->presence = [];


					// no te owner
					if (!isset($stall->active_owner[0])) {
						$stall->no_has_owner = 1;
					}
				}


				if ($absence = Absence::where('ownerstall_id', $stall->ownerstall_id)
										->where('start_at', '<=', $date." 00:00:00" )
										->where('end_at', '>=' , $date." 00:00:00" )
										->orderBy('created_at', 'desc')
										->first()) {
					$stall->has_absence = 1;
				}
	            	

            	$resposta['stalls'][] = $stall;
            }



			$resposta['res'] = 'ok';
		}else{
			//No estem en dia de mercat
			$resposta['res'] = 'ko';
			$resposta['msg'] = 'No estem en dia de mercat';
		}
		


		return Response::json($resposta);
	}
}

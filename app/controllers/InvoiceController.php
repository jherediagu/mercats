<?php


use Carbon\Carbon;

class InvoiceController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $remembertoken = Auth::user()->remember_token;

        $invoices = Invoice::whereHas('OwnerStall', function($q)
                {
                    $q->whereHas('Stall', function($q)
                    {
                        $q->where('market_id', Session::get('market'));
                    });
                })
                ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')
                ->get();


        return View::make('private.invoice.index')
                        ->with('invoices', $invoices)
                        ->with('token', $remembertoken);
    }




    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexNotPaid()
    {


        $date_1month = Carbon::now()->subMonths(1);

        $remembertoken = Auth::user()->remember_token;

        $invoices = Invoice::whereHas('OwnerStall', function($q)
                {
                    $q->whereHas('Stall', function($q)
                    {
                        $q->where('market_id', Session::get('market'));
                    });
                })
                ->where('paid', NULL)
                ->where('month','<=',$date_1month)
                ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')
                ->get();


        return View::make('private.invoice.invoice_not_paid')
                        ->with('invoices', $invoices)
                        ->with('token', $remembertoken);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('private.invoice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = ['month' => 'required|date']; //check if invoices not exists & calendar has days
        $nice = ['month' => 'data'];
            
        $validation = Validator::make(Input::all(), $rules);
        $validation->setAttributeNames($nice);
        
        if ($validation->fails())
        {
            return Redirect::route('calendar.index')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $from = date('Y-m-01 00:00:00', strtotime(Input::get('month')));
            $to = date('Y-m-01 00:00:00', strtotime("+1 months", strtotime($from)));
            $market = Session::get('market');
            
            $nq = DB::raw("(select count(*) 
                        from calendar
                        where market_id = {$market}
                        and date >= '{$from}'
                        and date < '{$to}'
                        and date >= `ownerstall`.`discharge_date`
                        and date < IFNULL(`ownerstall`.`deactivated_at`, '{$to}' ) ) as days");

            $nq1 = DB::raw("(select count(*) 
                        from calendar
                        where market_id = {$market}
                        and date >= '{$from}'
                        and date < '{$to}'
                        and date >= `ownerstall`.`discharge_date`
                        and date < IFNULL(`ownerstall`.`deactivated_at`, '{$to}' ) )");

            $ownerstalls = OwnerStall::select([$nq, 'ownerstall.*'])
                    ->whereHas('stall', function($q)
                    {
                        $q->where('market_id', Session::get('market'))->where('active',1);
                    })
                    ->where(function($query) use ($nq1)
                    {
                        $query->where($nq1, '>', 0);
                    })
                    ->get();

            $cnt = 0;
            $cnt2 = 0;
            foreach ($ownerstalls as $value)
            {
                if (Invoice::where('ownerstall_id', $value->id)
                        ->where('month', 'like', date('Y-m', strtotime(Input::get('month'))) . "-01%")
                        ->count() == 0)
                {
                    $cnt++;
                    $invoice = new Invoice();
                    $invoice->ownerstall_id = $value->id;
                    $invoice->num = rand(1000, 9999);
                    $invoice->month = date('Y-m', strtotime(Input::get('month'))) . "-01";
                    $invoice->qty = $value->days;
                    $invoice->price = $value->tariff->price;
                    $invoice->total = ($value->days * $value->stall->length * $value->tariff->price);
                    $invoice->save();
                }
                else $cnt2++;
            }

            return Redirect::route('invoice.index')
                            ->with(array('success_msg' => "{$cnt} rebuts afegits | {$cnt2} no afegits (ja existents)."));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $invoice = Invoice::whereHas('OwnerStall', function($q)
                    {
                        $q->whereHas('Stall', function($q)
                        {
                            $q->where('market_id', Session::get('market'));
                        });
                    })->findOrFail($id);

        return View::make('private.invoice.show')
                        ->with('invoice', $invoice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $invoice = Invoice::whereHas('OwnerStall', function($q)
                    {
                        $q->whereHas('Stall', function($q)
                        {
                            $q->where('market_id', Session::get('market'));
                        });
                    })->findOrFail($id);

        return View::make('private.invoice.edit')
                        ->with('invoice', $invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $invoice = Invoice::whereHas('OwnerStall', function($q)
                    {
                        $q->whereHas('Stall', function($q)
                        {
                            $q->where('market_id', Session::get('market'));
                        });
                    })->findOrFail($id);
                    
        $invoice->claimed = Input::get('claimed');
        $invoice->save();
        
        return Redirect::route('invoice.index')
                            ->with(array('success_msg' => 'Rebut editat correctament.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::whereHas('OwnerStall', function($q)
                    {
                        $q->whereHas('Stall', function($q)
                        {
                            $q->where('market_id', Session::get('market'));
                        });
                    })->findOrFail($id);
        $invoice->delete();
        
        return Redirect::route('invoice.index')
                            ->with(array('success_msg' => "Rebut eliminat correctament."));
    }

    /**
     * Print the specified resource and update as paid.
     *
     * @param  int  $id
     * @return Response
     */
    public function printout($id)
    {
        $invoice = Invoice::whereHas('OwnerStall', function($q)
                    {
                        $q->whereHas('Stall', function($q)
                        {
                            $q->where('market_id', Session::get('market'));
                        });
                    })->findOrFail($id);

        /*
        if (is_null($invoice->paid))
        {
            $invoice->paid = date('Y-m-d h:i:s');
            $invoice->save();
        }
        */

        return View::make('private.invoice.printout')
                        ->with('invoice', $invoice);
    }


    /** Update as paid the specified resource
    *
    * @param int $id
    * @return Response
    */
    public function payprint($id)
    {
         $invoice = Invoice::whereHas('OwnerStall', function($q)
                    {
                        $q->whereHas('Stall', function($q)
                        {
                            $q->where('market_id', Session::get('market'));
                        });
                    })->findOrFail($id);

        if (is_null($invoice->paid))
        {
            $invoice->paid = date('Y-m-d h:i:s');
            $invoice->save();
        }

        return Response::json(['res' => 'ok']);
    }



    /** Update as paid and return json to the app to print the resource
    *
    * @param int $id
    * @return JSON Response
    */
    public function payprintapp() {
    
        $id = Input::get('id_missatge');
        $token = Input::get('token');



        if (User::where('remember_token', '=', $token)->count() > 0) {
           // user token found


          /*  $invoice = Invoice::whereHas('OwnerStall', function($q)
                        {
                            $q->whereHas('Stall', function($q)
                            {
                                $q->where('market_id', Session::get('market'));
                            });
                        })->findOrFail($id);*/   

            // dia del mercat
            // data created_at  

            $invoice = Invoice::with('ownerstall')->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')->findOrFail($id);


            if (is_null($invoice->paid))
            {
                $dataPagat = date('Y-m-d h:i:s');
                $invoice->paid = $dataPagat;
                $invoice->save();
                $esCopia = 0;
            }
            else
            {
                $esCopia = 1;
            }
            

            //Definim variables titular        
            $titular = $invoice->ownerstall->owner->name;
            $address = $invoice->ownerstall->owner->address;
            $dni = $invoice->ownerstall->owner->dni;

            //Definim variables rebut
            $preuXmetre = $invoice->price;
            $qtyParades = $invoice->qty;
            $dataPagat = $invoice->paid;
            $numParada = $invoice->ownerstall->stall->num;
            $metresParada = $invoice->ownerstall->stall->length;
            $total = $invoice->total;
            $dataPagat = $invoice->paid;
            $numRebut = $invoice->num;
            $mes = $invoice->month;
            $diaMercat = $invoice->ownerstall->stall->market->name;

            $nomMercat = "Mercat de Manlleu"; //Falta definir


     
            $arrayPrinter = Array(
                "name" =>$titular,
                "address" =>$address,
                "dni" =>$dni,
                "preuXmetre" =>$preuXmetre,
                "metresParada" => $metresParada,
                "numParada" => $numParada,
                "total" => $total,
                "dataPagat" => $dataPagat,
                "numRebut" => $numRebut,
                "mes" => $mes,
                "diaMercat" => $diaMercat,
                "nomMercat" => $nomMercat,
                "qtyParades" => $qtyParades,
                "diaMercat" => $diaMercat,
                "esCopia" => $esCopia,
                "phone" => "690626327"

            );
            

        }else{

            $arrayPrinter = Array( "error" => "token incorrecte");
        }

        return Response::json($arrayPrinter);

    }


    /** Update as paid and return printed invoice
    *
    * @param int $id
    * @return JSON Response
    */
    public function printandpay($id) {
    
    
            $invoice = Invoice::with('ownerstall')->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')->findOrFail($id);

            if (is_null($invoice->paid))
            {
                $dataPagat = date('Y-m-d h:i:s');
                $invoice->paid = $dataPagat;
                $invoice->save();
                $esCopia = 0;
            }
            else
            {
                $esCopia = 1;
            }
            
            //Definim variables titular        
            $titular = $invoice->ownerstall->owner->name;
            $address = $invoice->ownerstall->owner->address;
            $dni = $invoice->ownerstall->owner->dni;

            //Definim variables rebut
            $preuXmetre = $invoice->price;
            $qtyParades = $invoice->qty;
            $dataPagat = $invoice->paid;
            $numParada = $invoice->ownerstall->stall->num;
            $metresParada = $invoice->ownerstall->stall->length;
            $total = $invoice->total;
            $dataPagat = $invoice->paid;
            $numRebut = $invoice->num;
            $mes = $invoice->month;
            $diaMercat = $invoice->ownerstall->stall->market->name;

            $nomMercat = "Mercat de Manlleu"; //Falta definir


     
            $arrayPrinter = Array(
                "name" =>$titular,
                "address" =>$address,
                "dni" =>$dni,
                "preuXmetre" =>$preuXmetre,
                "metresParada" => $metresParada,
                "numParada" => $numParada,
                "total" => $total,
                "dataPagat" => $dataPagat,
                "numRebut" => $numRebut,
                "mes" => $mes,
                "diaMercat" => $diaMercat,
                "nomMercat" => $nomMercat,
                "qtyParades" => $qtyParades,
                "diaMercat" => $diaMercat,
                "esCopia" => $esCopia,
                "phone" => "690626327"

            );
            

            $pdf = App::make('dompdf');
            $pdf = PDF::loadView('pdf.invoice', $arrayPrinter);
            return $pdf->stream();
        //return Response::json($arrayPrinter);

    }
}

<?php

class SectorController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('private.sector.index')
                        ->with('sectors', Sector::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('private.sector.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = Validator::make(Input::all(), ['name' => 'required']);

        if ($validation->fails())
        {
            return Redirect::route('sector.create')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $sector = new Sector(Input::all());
            $sector->save();

            return Redirect::route('sector.index')
                            ->with(array('success_msg' => 'Sector afegit correctament.'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $authprod = DB::table('auth_prod')->get();
        $rel_prod_sectors = DB::table('rel_prod_sectors')
                                    ->where('id_sector', $id)
                                    ->lists( 'id_product' );


        return View::make('private.sector.show')
                        ->with('sector', Sector::findOrFail($id))
                        ->with('authprod',$authprod)
                        ->with('rel_prod_sectors',$rel_prod_sectors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return View::make('private.sector.edit')
                        ->with('sector', Sector::findOrFail($id));
    }

    /**
     * Update the products in sector.
     *
     * @param  int  $id
     * @return Response
     */
    public function editProducts($id)
    {
        $id_sector = $id;
        $id_product = Input::get('id_product');

        DB::table('rel_prod_sectors')->insert(
            array('id_sector' => $id_sector, 
                  'id_product'=> $id_product)
        );
                
        
        return;
    }


    /**
     * Delete the products in sector.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteProducts($id)
    {
        $id_sector = $id;
        $id_product = Input::get('id_product');


        DB::table('rel_prod_sectors')->where('id_sector', '=', $id_sector)
                                     ->where('id_product', '=', $id_product)
                                     ->delete();
                        
        return;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // relacio auth_prod amb sectors
        // $auth_prod = DB::table('rel_prod_sectors')->get();

        $sector = Sector::findOrFail($id);
        
        $validation = Validator::make(Input::all(), ['name' => 'required']);

        if ($validation->fails())
        {
            return Redirect::route('sector.edit')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $sector->update(Input::all());

            return Redirect::route('sector.index')
                            ->with(array('success_msg' => 'Sector editat correctament'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $sector = Sector::findOrFail($id);
        
        if (Stall::where('sector_id', $id)->count() > 0)
              return Redirect::route('sector.index')
                ->with(array('error_msg' => 'No es pot eliminar el sector.'));
        else 
        {
            $sector->delete();
            return Redirect::route('sector.index')
                ->with(array('success_msg' => 'Sector eliminat correctament.'));
        }
        
        
    }

}

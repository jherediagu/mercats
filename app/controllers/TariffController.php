<?php

class TariffController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tariff = Tariff::where('market_id', Session::get('market'))->get();
                
                return View::make('private.tariff.index')
                        ->with('tariff', $tariff);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$tariff = Tariff::where('market_id', Session::get('market'))->findOrFail($id);

        return View::make('private.tariff.create');                    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		
		$rules = [  'name' => 'required|between:1,100', 
                        'price' => 'required|numeric' ];
            $validation = Validator::make(Input::all(), $rules);
        
            if ($validation->fails())
            {
                return Redirect::route('tariff.create')
                                ->withErrors($validation)
                                ->withInput();
            }
            else
            {
                $tariff = new Tariff(Input::all());
                $tariff->market_id = Session::get('market');
                $tariff->save();
                return Redirect::route('tariff.index')->with(array('success_msg' => 'Tarifa creada correctament'));
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $tariff = Tariff::where('market_id', Session::get('market'))->findOrFail($id);

            return View::make('private.tariff.edit')
                    ->with('tariff', $tariff);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $rules = [  'name' => 'required|between:1,100', 
                        'price' => 'required|numeric' ];
            $validation = Validator::make(Input::all(), $rules);
        
            if ($validation->fails())
            {
                return Redirect::route('tariff.edit', array($id))
                                ->withErrors($validation)
                                ->withInput();
            }
            else
            {
                $tariff = Tariff::where('market_id', Session::get('market'))->findOrFail($id);
                $tariff->update(Input::all());

                return Redirect::route('tariff.index')->with(array('success_msg' => 'Tarifa editada correctament'));
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}

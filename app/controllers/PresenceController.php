<?php

class PresenceController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexPresence()
    {
	$np = Array();
	$np2 = Array();
        $calendar = Calendar::where('market_id', Session::get('market'))
                        ->orderBy('date', 'asc')
                        ->get();
        
        $ownerstall = OwnerStall::whereHas('stall', function($q){
                            $q->where('market_id', Session::get('market'));
                        })
                        ->with('owner')
                        ->get(); 
        
        foreach ($calendar as $day)
        {
            $date = date('Y-m-d', strtotime($day->date));
            
            foreach ($ownerstall as $os)
            {
                $start = date('Y-m-d', strtotime($os->discharge_date));
                $end = is_null($os->deactivated_at) ? NULL : date('Y-m-d', strtotime($os->deactivated_at));
                
                if (($date >= $start && $date <= $end && !is_null($end)) || ($date >= $start && is_null($end)))
                    $np["{$day->id}-{$os->stall_id}"] = $os;
            }
        }

        $stall = Stall::where('market_id', Session::get('market'))
                        ->get();
        
        $presence = Presence::whereHas('calendar', function($q){
                        $q->where('market_id', Session::get('market'));
                    })
                    ->with('ownerstall')
                    ->get(); 
               
        foreach ($presence as $value)
            $np2["{$value->calendar_id}-{$value->ownerstall->stall_id}"] = 1;
            
        
        return View::make('private.presence.index')
                        ->with('calendar', $calendar)
                        ->with('stall', $stall)
                        ->with('np', $np)
                        ->with('np2', $np2);
        
        
    }
    
    /**
     * Display a listing of the resource Presence.
     *
     * @return Response
     */

    public function index()
    {
    $np = Array();
    $np2 = Array();
        $calendar = Calendar::where('market_id', Session::get('market'))
                        ->orderBy('date', 'asc')
                        ->get();
        
        $ownerstall = OwnerStall::whereHas('stall', function($q){
                            $q->where('market_id', Session::get('market'));
                        })
                        ->with('owner')
                        ->get(); 
        
        foreach ($calendar as $day)
        {
            $date = date('Y-m-d', strtotime($day->date));
            
            foreach ($ownerstall as $os)
            {
                $start = date('Y-m-d', strtotime($os->discharge_date));
                $end = is_null($os->deactivated_at) ? NULL : date('Y-m-d', strtotime($os->deactivated_at));
                
                if (($date >= $start && $date <= $end && !is_null($end)) || ($date >= $start && is_null($end)))
                    $np["{$day->id}-{$os->stall_id}"] = $os;
            }
        }

        $stall = Stall::where('market_id', Session::get('market'))
                        ->get();
        
        $presence = Presence::whereHas('calendar', function($q){
                        $q->where('market_id', Session::get('market'));
                    })
                    ->with('ownerstall')
                    ->get(); 
               
        foreach ($presence as $value)
            $np2["{$value->calendar_id}-{$value->ownerstall->stall_id}"] = 1;
            
        
        return View::make('private.presence.indexPresence')
                        ->with('calendar', $calendar)
                        ->with('stall', $stall)
                        ->with('np', $np)
                        ->with('np2', $np2);
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //check ownerstall is active in given date
        
        $date = is_null(Calendar::where('market_id', '=', Session::get('market'))
                                ->find(Input::get('calendar_id'))) ? Calendar::where('market_id', '=', Session::get('market'))
                        ->where('date', '>=', date('Y/m/d'))
                        ->first() : Calendar::where('market_id', '=', Session::get('market'))
                        ->find(Input::get('calendar_id'));

        $ownerstall = OwnerStall::// whereNull('deactivated_at') ask if is allowed to create deactive ownerstalls
                        whereHas('stall', function($q)
                        {
                            $q->where('market_id', '=', Session::get('market'));
                        })
                        ->find(Input::get('ownerstall_id'));

        $calendar = Calendar::where('market_id', '=', Session::get('market'))->get();

        return View::make('private.presence.create')
                        ->with('calendar', $calendar)
                        ->with('ownerstall', $ownerstall)
                        ->with('date', $date);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //check for duplicate presence values
        $nice = ['ownerstall_id' => 'parada',
                'date' => 'data'];
        
        $rules = [  'ownerstall_id' => 'required', //check belongs current market and is active
                    'date' => 'required|date|validPresence:' . Input::get('ownerstall_id')]; //check exists & belongs current market

        $validation = Validator::make(Input::all(), $rules);
        $validation->setAttributeNames($nice);
        
        if ($validation->fails())
        {
            return Redirect::route('presence.create', ['ownerstall_id' => Input::get('ownerstall_id')])
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $presence = new Presence(Input::all());
            $presence->calendar_id = Calendar::where('date', 'like', date('Y-m-d', strtotime(Input::get('date'))) . "%")
                                            ->where('market_id', '=', Session::get('market'))
                                            ->firstOrFail()->id;
            $presence->save();
            
            return Redirect::route('presence.index')
                            ->with(array('success_msg' => 'Assistència afegida correctament.'));
        }
    }



    /* Posar o treure presència */
    public function togglePresence(){

        //Comprovar si existeix per data d'avui i mercat
        $valor = Input::get('valor');
		
		if ($registre = Presence::where('calendar_id', Input::get('calendar_id'))->where('ownerstall_id', Input::get('ownerstall_id'))->first())        {
            $existeix = true;
        }else{
            $existeix = false;
        }					

		if($valor == 'si'){
			
            //Si existeix, eliminar
            
            if ($existeix == true){
                $presence = Presence::where('calendar_id', Input::get('calendar_id'))
                        ->where('ownerstall_id', Input::get('ownerstall_id')) 
                        ->delete();
            }

            $resposta['res'] = 'oksi';

		}elseif($valor == 'no'){
			//Si no existeix, fer insert
            if ($existeix == false){
            
                $presence = new Presence();
                $presence->ownerstall_id = Input::get('ownerstall_id');
                $presence->calendar_id = Input::get('calendar_id');
                $presence->save();
            }

            $resposta['res'] = 'okno';
		}else{

			$resposta['res'] = 'ko';

		}

        return Response::json($resposta);
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}

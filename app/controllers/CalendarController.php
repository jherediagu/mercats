<?php

use Carbon\Carbon;

class CalendarController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $days = Calendar::where('market_id', '=', Session::get('market'))->get();

        if (Input::get('view') == 'calendar')
            return View::make('private.calendar.index-calendar')
                            ->with('days', $days);
        else
            return View::make('private.calendar.index-list')
                            ->with('days', $days);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('private.calendar.create')
                        ->with('daysofweek', ['Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte','Diumenge']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store()
    {
        
        $inputs = Input::all();
        $rules = [ 'days' => 'required|array']; //max length?
        
        foreach ((array) Input::get('days') as $key => $value)
        {
            $rules["day-{$key}"] = "date|unique_composite:calendar,date,market_id," . Session::get('market'); //custom validation: both market_id and date = unique
            $inputs["day-{$key}"] = date('Y-m-d', strtotime($value)) . ' 00:00:00';
            $niceName["day-{$key}"] = "data {$value}";
        }

        $validation = Validator::make($inputs, $rules);
        $validation->setAttributeNames($niceName); 


        if ($validation->fails())
        {
            return Redirect::route('calendar.create')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            foreach (Input::get('days') as $value)
            {
                $calendar = new Calendar();
                $calendar->date = $value;
                $calendar->market_id = Session::get('market');
                $calendar->save();
            }
            return Redirect::route('calendar.index')
                            ->with(array('success_msg' => 'Datas afegides correctament.'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $day = Calendar::findOrFail($id);

        $presence = OwnerStall::whereHas('stall', function($q) {
                            $q->where('market_id', Session::get('market'));
                        })
                        ->whereHas('presence', function($q) use ($id) {
                            $q->where('calendar_id', $id);
                        })
                        ->with('presence')
                        ->with('owner')
                        ->with('stall')
                        ->get();

        $missing = [];
//        $missing = OwnerStall::where('deactivated_at', null)
//                        ->whereHas('stall', function($q) {
//                            $q->where('market_id', Session::get('market'));
//                        })
//                        ->whereHas('presence', function($q) use ($id) {
//                            $q->where('calendar_id', $id);
//                        }, 0)
//                        ->with('presence')
//                        ->with('owner')
//                        ->with('stall')
//                        ->get();
          
        $general = Note::where('calendar_id', $id)
                        ->whereNull('ownerstall_id')
                        ->where('market_id', Session::get('market'))
                        ->get();
        
        $notes = Note::where('calendar_id', $id)
                        ->whereNotNull('ownerstall_id')
                        ->where('market_id', Session::get('market'))
                        ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')
                        ->get();
        
        $absences = Absence::whereRaw("'".date("Y-m-d", strtotime($day->date))."' between `start_at` and `end_at`")
                            ->whereHas('ownerstall', function($q) {
                            $q->whereHas('stall', function($q){
                                $q->where('market_id', Session::get('market'));
                            });
                        })
                        ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')
                        ->get();
                        
        return View::make('private.calendar.show')
                        ->with('day', $day)
                        ->with('presence', $presence)
                        ->with('missing', $missing)
                        ->with('general', $general)
                        ->with('notes', $notes)
                        ->with('absences', $absences);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $date = Calendar::where('market_id', Session::get('market'))->findOrFail($id);
        try 
        {
            $date->delete();
            
            return Redirect::route('calendar.index')
                ->with(array('success_msg' => 'Data eliminada correctament.'));
        } 
        catch ( Illuminate\Database\QueryException $e) 
        {
            return Redirect::route('calendar.index')
                ->with(array('error_msg' => 'No es pot eliminar la data.'));
        }
    }

}

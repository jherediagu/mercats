<?php

class AuthProdController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $authprod = AuthProd::where('market_id', Session::get('market'))->get();
        
        return View::make('private.authprod.index')
                        ->with('authprods', $authprod);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('private.authprod.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = Validator::make(Input::all(), ['name' => 'required']);

        if ($validation->fails())
        {
            return Redirect::route('authprod.create')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $authprod = new AuthProd(Input::all());
            $authprod->market_id = Session::get('market');
            $authprod->save();

            return Redirect::route('authprod.index')
                            ->with(array('success_msg' => 'Producte autoritzat afegit correctament'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $sectors = DB::table('sector')->lists('name','id');

        $rel_prod_sectors = DB::table('rel_prod_sectors')
                                    ->where('id_product', $id)->select('id_sector')->get();

        return View::make('private.authprod.show')
                        ->with('products',$rel_prod_sectors)
                        ->with('sectors',$sectors);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $authprod = AuthProd::where('market_id', Session::get('market'))->findOrFail($id);
        
        return View::make('private.authprod.edit')
                        ->with('authprod', $authprod);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $authprod = AuthProd::where('market_id', Session::get('market'))->findOrFail($id);
        
        $validation = Validator::make(Input::all(), ['name' => 'required']);

        if ($validation->fails())
        {
            return Redirect::route('authprod.edit')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $authprod->market_id = Session::get('market');
            $authprod->update(Input::all());

            return Redirect::route('authprod.index')
                            ->with(array('success_msg' => 'Producte autoritzat editat correctament.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $authprod = AuthProd::where('market_id', Session::get('market'))->findOrFail($id);

        if (Stall::where('auth_prod_id', $id)->count() > 0)
        {
              return Redirect::route('authprod.index')
                ->with(array('error_msg' => 'No es pot eliminar el producte autoritzat.'));
        }
        else 
        {
            $authprod->delete();
            return Redirect::route('authprod.index')
                ->with(array('success_msg' => 'Producte autoritzat eliminat correctament.'));
        }
    }


    /**
     * Show relation with sectors and auth products.
     *
     * @param  int  $id
     * @return Response
     */
    public function relation_sector_products($id)
    {

        $authprod   = DB::table('auth_prod')->where('id', $id)->first();

        $stalls     = DB::table('stall')->lists('sector_id','id');
        $rel_prod   = DB::table('rel_prod_sectors')->where('id_product', $id)->get();

        return View::make('private.authprod.showsectors')->with('stalls',$stalls)
                                                         ->with('rel_prod',$rel_prod)
                                                         ->with('authprod',$authprod);
        
    }


}

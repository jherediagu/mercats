<?php

class UserController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::where('town_id', Auth::User()->town_id)
                ->where('role', '!=', 'admin')
                ->get();

        return View::make('private.user.index')
                        ->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $markets = Market::where('town_id', Auth::User()->town_id)->lists('name', 'id');

        
        return View::make('private.user.create')
                        ->with('markets', $markets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $nice = ['name' => 'nom',
                'surname' => 'congom',
                'email' => 'correu electrònic',
                'dni' => 'dni',
                'password' => 'contrasenya',
                'markets' => 'mercats',
                'role' => 'rol'];
        $inputs = Input::all();
        $rules = [  'name' => 'required|between:1,100', 
                    'surname' => 'between:1,100', 
                    'email' => 'email|between:1,100|unique:users,email', 
                    'dni' => 'required|unique:users,dni|between:9,9', 
                    'password' => 'required|confirmed|between:6,8',
                    'markets' => 'required|array',
                    'role' => 'required|in:int-tab,int,tab'];
        
        foreach ((array)Input::get('markets') as $key => $value)
        {
            $inputs["market-{$key}"] = $value;
            $rules["market-{$key}"] = 'exists:market,id';
        }
        
        $validation = Validator::make($inputs, $rules);
        $validation->setAttributeNames($nice);

        
        if ($validation->fails())
        {
            return Redirect::route('user.create')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $user = new User(Input::all());
            $user->town_id = Auth::User()->town_id;
            $user->password = Hash::make(Input::get("password"));
            $user->save();
            
            $user->markets()->attach(Input::get('markets'));

            
            

            return Redirect::route('user.index')
                            ->with(array('success_msg' => 'Usuari afegit correctament'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        die();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $markets = Market::where('town_id', Auth::User()->town_id)->lists('name', 'id');
        
        return View::make('private.user.edit')
                        ->with('user', User::findOrFail($id))
                        ->with('markets', $markets);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $nice = ['name' => 'nom',
                'surname' => 'congom',
                'email' => 'correu electrònic',
                'dni' => 'dni',
                'password' => 'contrasenya',
                'markets' => 'mercats',
                'role' => 'rol'];
        
        $user = User::findOrFail($id);
        
        $inputs = Input::all();
        $rules = [  'name' => 'required|between:1,100', 
                    'surname' => 'between:1,100', 
                    'email' => 'email|between:1,100', 
                    'dni' => "required|unique:users,dni,{$id}|between:9,9", 
                    'markets' => 'required|array',
                    'role' => 'required|in:int-tab,int,tab'];
        
        foreach ((array)Input::get('markets') as $key => $value)
        {
            $inputs["market-{$key}"] = $value;
            $rules["market-{$key}"] = 'exists:market,id';
        }
        
        $validation = Validator::make($inputs, $rules);
        $validation->setAttributeNames($nice);
        
        if ($validation->fails())
        {
            return Redirect::route('user.edit', array($id))
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            
            $user->update(Input::all());

            $user->markets()->sync(Input::get('markets'));
            
            return Redirect::route('user.index')->with(array('success_msg' => 'Usuari editat correctament'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return Redirect::route('user.index')
                ->with(array('success_msg' => 'Usuari eliminat correctament'));
    }

}

<?php

class OwnerController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$owners = Owner::has('stalls')->get();
        $owners = Owner::get();

        return View::make('private.owner.index')
                        ->with('owners', $owners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('private.owner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $inputs = Input::all();
        
        $nice = ['name' => 'nom',
                'address' => 'direcció',
                'email' => 'correu electrònic',
                'dni' => 'dni',
                'phone' => 'telèfon',
                'iban' => 'iban',
                'image' => 'foto',
                'pdf1' => 'Alta AIE',
                'pdf2' => 'Alta autònoms',
                'sname' => 'nom del suplent',
                'saddress' => 'direcció del suplent',
                'semail' => 'correu del suplent',
                'sdni' => 'dni del suplent',
                'sphone' => 'teléfon del suplent',
                'pdf3' => 'contracte de treball'];
        
        $rules = [ 'name' => 'required|between:1,100',
            'address' => 'required|between:1,500',
            'email' => 'email|unique:owner,email|between:1,100',
            'dni' => 'required|unique:owner,dni|between:9,9',
            'phone' => 'required|digits_between:9,9',
            'iban' => 'required', //think about check it
            'image' => 'image',
            'pdf1' => 'mimes:pdf',
            'pdf2' => 'mimes:pdf',
            'sname' => 'between:1,100',
            'saddress' => 'between:1,500',
            'semail' => 'email|between:1,100',
            'sdni' => 'between:9,9',
            'sphone' => 'digits_between:9,9',
            'pdf3' => 'mimes:pdf'];

        $validation = Validator::make($inputs, $rules);
        $validation->setAttributeNames($nice);


        if ($validation->fails())
        {
            return Redirect::route('owner.create')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $owner = new Owner(Input::all());

            if (Input::hasFile('image') && Input::file('image')->isValid())
            {
                $file = Input::file('image');
                $owner->image = strtolower('user-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("imgs", $owner->image);
            }
            
            if (Input::hasFile('pdf1') && Input::file('pdf1')->isValid())
            {
                $file = Input::file('pdf1');
                $owner->pdf1 = strtolower('pdf1-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("pdf", $owner->pdf1);
            }
            
            if (Input::hasFile('pdf2') && Input::file('pdf2')->isValid())
            {
                $file = Input::file('pdf2');
                $owner->pdf2 = strtolower('pdf2-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("pdf", $owner->pdf2);
            }
            
            if (Input::hasFile('pdf3') && Input::file('pdf3')->isValid())
            {
                $file = Input::file('pdf3');
                $owner->pdf3 = strtolower('pdf3-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("pdf", $owner->pdf3);
            }

            $owner->save();

            return Redirect::route('owner.index')
                            ->with(array('success_msg' => 'Titular afegit correctament'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $owner = Owner::findOrFail($id);
        
        $ownerstall = OwnerStall::where('owner_id', '=', $id)
                ->whereHas('stall', function($q)
                {
                    $q->where('market_id', Session::get('market'));
                })
                ->get();
                
        $presence = Presence::whereHas('ownerstall', function ($q) use($id)
                {
                    $q->whereHas('stall', function($q) use($id)
                            {
                                $q->where('market_id', Session::get('market'));
                            })
                            ->where('owner_id', $id);
                })
                ->with('ownerstall', 'ownerstall.stall', 'calendar')
                ->get();
                
        $notes = Note::whereHas('ownerstall', function ($q) use($id)
                {
                    $q->where('owner_id', $id);
                })
                ->where('market_id', Session::get('market'))
                ->with('ownerstall', 'ownerstall.stall')
                ->get();
        $general = Note::whereNull('ownerstall_id')
                ->where('market_id', Session::get('market'))
                ->with('ownerstall', 'ownerstall.stall')
                ->get();
                
        $absence = Absence::whereHas('ownerstall', function ($q) use($id)
                {
                    $q->whereHas('stall', function($q) use($id)
                            {
                                $q->where('market_id', Session::get('market'));
                            })
                            ->where('owner_id', $id);
                })
                ->with('ownerstall', 'ownerstall.stall')
                ->get();
        
        $invoices = Invoice::whereHas('OwnerStall', function($q) use($id)
                {
                    $q->whereHas('stall', function($q)
                    {
                        $q->where('market_id', Session::get('market'));
                    })
                    ->where('owner_id', $id);
                })
                ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')
                ->get();
                
        return View::make('private.owner.show')
                        ->with('owner', $owner)
                        ->with('ownerstalls', $ownerstall)
                        ->with('presence', $presence)
                        ->with('notes', $notes)
                        ->with('general', $general)
                        ->with('absences', $absence)
                        ->with('invoices', $invoices);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return View::make('private.owner.edit')
                        ->with('owner', Owner::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $nice = ['name' => 'nom',
                'address' => 'direcció',
                'email' => 'correu electrònic',
                'dni' => 'dni',
                'phone' => 'telèfon',
                'iban' => 'iban',
                'image' => 'foto',
                'pdf1' => 'Alta AIE',
                'pdf2' => 'Alta autònoms',
                'sname' => 'nom del suplent',
                'saddress' => 'direcció del suplent',
                'semail' => 'correu del suplent',
                'sdni' => 'dni del suplent',
                'sphone' => 'teléfon del suplent',
                'pdf3' => 'contracte de treball'];
        
        $owner = Owner::findOrFail($id);

        $inputs = Input::all();
        $rules = [ 'name' => 'required|between:1,100',
            'address' => 'required|between:1,500',
            'email' => 'email|between:1,100',
            'dni' => "required|unique:owner,dni,{$id}|between:9,9",
            'phone' => 'required|digits_between:9,9',
            'iban' => 'required', //think about check it
            'image' => 'image',
            'pdf1' => 'mimes:pdf',
            'pdf2' => 'mimes:pdf',
            'sname' => 'between:1,100',
            'saddress' => 'between:1,500',
            'semail' => 'email|between:1,100',
            'sdni' => 'between:9,9',
            'sphone' => 'digits_between:9,9',
            'pdf3' => 'mimes:pdf'];


        $validation = Validator::make($inputs, $rules);
        $validation->setAttributeNames($nice);

        if ($validation->fails())
        {
            return Redirect::route('owner.edit', array($id))
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            if (Input::hasFile('image') && Input::file('image')->isValid())
            {
                File::delete(public_path() . "assets/imgs/owner/{$owner->image}");

                $file = Input::file('image');
                $owner->image = strtolower('user-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("imgs", $owner->image);
            }

            if (Input::hasFile('pdf1') && Input::file('pdf1')->isValid())
            {
                $file = Input::file('pdf1');
                $owner->pdf1 = strtolower('pdf1-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("pdf", $owner->pdf1);
            }
            
            if (Input::hasFile('pdf2') && Input::file('pdf2')->isValid())
            {
                $file = Input::file('pdf2');
                $owner->pdf2 = strtolower('pdf2-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("pdf", $owner->pdf2);
            }
            
            if (Input::hasFile('pdf3') && Input::file('pdf3')->isValid())
            {
                $file = Input::file('pdf3');
                $owner->pdf3 = strtolower('pdf3-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("pdf", $owner->pdf3);
            }
            
            $owner->update(Input::all());

            return Redirect::route('owner.index')
                            ->with(array('success_msg' => 'Titular editat correctamente'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return Redirect::route('owner.index');
    }

    public function ajaxSearch()
    {
        $owners = Owner::where('dni', 'like', '%' . Input::get('term') . '%')
                ->orWhere('name','like','%' . Input::get('term') . '%')
                ->get();

        return Response::json($owners);
    }

}

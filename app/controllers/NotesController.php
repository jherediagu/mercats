<?php

class NotesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $notes = Note::where('market_id', Session::get('market'))->get();
        
        return View::make('private.notes.index')
                ->with('notes', $notes)
                ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $date = is_null(Calendar::where('market_id', '=', Session::get('market'))
                                ->find(Input::get('calendar_id'))) 
                ? Calendar::where('market_id', '=', Session::get('market'))
                        ->where('date', '>=', date('Y/m/d'))
                        ->first() 
                : Calendar::where('market_id', '=', Session::get('market'))
                        ->find(Input::get('calendar_id'));

        $ownerstall = OwnerStall::where('deactivated_at', '=', null)
                            ->whereHas('stall', function($q)
                            {
                                $q->where('market_id', '=', Session::get('market'));
                            })
                            ->find(Input::get('ownerstall_id'));

        $calendar = Calendar::where('market_id', '=', Session::get('market'))->get();

        $note_emails = DB::table('note_emails')->lists('name','email');

        return View::make('private.notes.create')
                        ->with('calendar', $calendar)
                        ->with('ownerstall', $ownerstall)
                        ->with('note_emails', $note_emails)
                        ->with('date', $date);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $nice = ['ownerstall_id' => 'titular',
                'type' => 'tipus',
                'happens_at' => 'data',
                'title' => 'títol',
                'description' => 'descripció',
                'resolved' => 'resolt'];
        
        //check for duplicate presence values
        $rules = [  'type' => 'in:0,1,2',
                    'ownerstall_id' => 'validOwnerStall', //check belongs current market and is active
                    'happens_at' => 'required|date',
                    'title' => 'required|max:500',
                    'description' => 'required',
                    'resolved' => 'in:0,1',
                    'images' => 'image']; 

        $validation = Validator::make(Input::all(), $rules);
        $validation->setAttributeNames($nice);
        
        if ($validation->fails())
        {
            return Redirect::route('notes.create')
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $note = new Note(Input::all());

            
            $calendar = Calendar::where('date', 'like', date('Y-m-d', strtotime(Input::get('happens_at'))) . "%")
                                        ->where('market_id', '=', Session::get('market'))
                                        ->first();
            
            $note->calendar_id = !is_null($calendar) ? $calendar->id : NULL;
            $note->ownerstall_id = Input::get('type') == 0 ? Input::get('ownerstall_id') : NULL;
            $note->market_id = Session::get('market');
            $note->solved_at = Input::get('solved') == 0 ? date('Y/m/d h:i:s') : NULL;
            

            $type = Input::get('type');

            if ($type != 2) {

                $note->sent_to = Input::get('product_id');

            }

            if (Input::hasFile('images') && Input::file('images')->isValid())
            {
                $file = Input::file('images');
                $note->images = strtolower('notes-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("imgs/notes", $note->images);

                $image_name = $note->images;

                $image = 'imgs/notes/'.$image_name;

            }else {

                $image = "";
            }
            
            $note->save();

            $type = Input::get('type');

            $send_mail = Input::get('enviar_mail');

            if ($send_mail == 0) {
                // send email if check send email "Yes"

                if ($type != 2) {

                    $email = Input::get('product_id');

                    $data = array('email' => $email );
                    
                    $data_email = array(
                            'happens_at'    =>Input::get('happens_at'), 
                            'title'         =>Input::get('title'), 
                            'description'   =>Input::get('description'), 
                            'email'         =>Input::get('product_id'),
                            'image'         =>$image,


                        );

                        Mail::send('emails.incidencies', $data_email, function($message) use ($data)
                        {
                            $message->to($data['email'], 'Mercagest')->subject('Incidencia');
                            //$message->to('joanh@javajan.com', 'Mercagest')->subject('Incidencia');

                        });
                
                }

            }

                
            /*

            // get ownerstall id
            $ownerstall_id_note = Input::get('ownerstall_id');
            $owner_stall        = OwnerStall::find($ownerstall_id_note);
            $owner_id           = $owner_stall->owner_id;

            // get owner data
            $owner  = Owner::find($owner_id);

            */
                        
            return Redirect::route('notes.index')
                            ->with(array('success_msg' => 'Incidència afegida correctament'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $note = Note::where('market_id', Session::get('market'))->findOrFail($id);
        
        return View::make('private.notes.show')
                ->with('note', $note);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $note = Note::where('market_id', Session::get('market'))->findOrFail($id);
        //$note->happens_at = date('d-m-Y', strtotime($note->happens_at));
        
        $calendar = Calendar::where('market_id', '=', Session::get('market'))->get();
        
        return View::make('private.notes.edit')
                ->with('note', $note)
                ->with('calendar', $calendar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $nice = ['ownerstall_id' => 'titular',
                'type' => 'tipus',
                'happens_at' => 'data',
                'title' => 'títol',
                'description' => 'descripció',
                'resolved' => 'resolt'];
        
        //check for duplicate presence values
        $rules = [  'type' => 'in:0,1',
                    'ownerstall_id' => 'validOwnerStall', //check belongs current market and is active
                    'happens_at' => 'required|date',
                    'title' => 'required|max:500',
                    'description' => 'required',
                    'solved_at' => 'date',
                    'images' => 'image']; 

        $validation = Validator::make(Input::all(), $rules);
        $validation->setAttributeNames($nice);

        if ($validation->fails())
        {
            return Redirect::route('notes.edit', $id)
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $note = Note::findOrFail($id);
            
            $calendar = Calendar::where('date', 'like', date('Y-m-d', strtotime(Input::get('happens_at'))) . "%")
                                        ->where('market_id', '=', Session::get('market'))
                                        ->first();

            
            $note->fill(Input::all());
            
            $note->calendar_id = !is_null($calendar) ? $calendar->id : NULL;
            $note->ownerstall_id = Input::get('type') == 0 ? Input::get('ownerstall_id') : NULL;

            if (Input::hasFile('images') && Input::file('images')->isValid())
            {
                $file = Input::file('images');
                $note->images = strtolower('notes-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("imgs/notes", $note->images);
            }
            
            $note->save();
            
            return Redirect::route('notes.index')
                            ->with(array('success_msg' => 'Incidència editada correctament.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        $note = Note::where('market_id', Session::get('market'))->findOrFail($id);

      
            $note->delete();
            return Redirect::route('notes.index')
                ->with(array('success_msg' => 'Incidencia eliminada correctament.'));
        
    }

}

<?php

class StallController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $stalls = Stall::where('market_id', '=', Session::get('market'))
                ->with('authprod', 'sector', 'activeOwner')
                ->get();

        return View::make('private.stall.index')
                        ->with('stalls', $stalls);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $stall = Stall::where('id', '=', $id)
                ->where('market_id', '=', Session::get('market'))
                ->firstOrFail();

        $ownerstall = OwnerStall::where('stall_id', '=', $id)
                ->whereHas('stall', function($q)
                {
                    $q->where('market_id', Session::get('market'));
                })
                ->get();

        $presence = Presence::whereHas('ownerstall', function ($q) use($id)
                {
                    $q->whereHas('stall', function($q) use($id)
                    {
                        $q->where('market_id', Session::get('market'));
                    })
                    ->where('stall_id', $id);
                })
                ->with('ownerstall', 'ownerstall.stall', 'calendar')
                ->get();

        $notes = Note::whereHas('ownerstall', function ($q) use($id)
                {
                    $q->where('stall_id', $id);
                })
                ->where('market_id', Session::get('market'))
                ->with('ownerstall', 'ownerstall.stall')
                ->get();
        
        $general = Note::whereNull('ownerstall_id')
                ->where('market_id', Session::get('market'))
                ->with('ownerstall', 'ownerstall.stall')
                ->get();

        $absence = Absence::whereHas('ownerstall', function ($q) use($id)
                {
                    $q->whereHas('stall', function($q) use($id)
                    {
                        $q->where('market_id', Session::get('market'));
                    })
                    ->where('stall_id', $id);
                })
                ->with('ownerstall', 'ownerstall.stall')
                ->get();

        $invoices = Invoice::whereHas('OwnerStall', function($q) use($id)
                {
                    $q->whereHas('stall', function($q)
                    {
                        $q->where('market_id', Session::get('market'));
                    })
                    ->where('stall_id', $id);
                })
                ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')
                ->get();

        return View::make('private.stall.show')
                        ->with('stall', $stall)
                        ->with('ownerstalls', $ownerstall)
                        ->with('presence', $presence)
                        ->with('notes', $notes)
                        ->with('general', $general)
                        ->with('absences', $absence)
                        ->with('invoices', $invoices);
    }

    /**
    * Display info.
    *
    * @param  int  $id
    * @return Response
    */
    public function getInfo(){

        if(Input::has('market')){
            $market_id = Input::get('market');
            Session::put('market', $market_id);
        }else{
            $market_id = Session::get('market');
        }

        $response['market_id'] = $market_id;
        $response['num'] = Input::get('num');

        $stall = Stall::where('market_id', $market_id)
                    ->where('num', Input::get('num'))
                    ->orderBy('id', 'asc')
                    ->firstOrFail();
                
        $route = 'stall/' . $stall->id;
        return Redirect::to($route);
    }


    /**
    * Display info.
    *
    * @param  int  $id
    * @return Response
    */
    public function editStall($id){

        $stall = Stall::where('id', '=', $id)
                ->where('market_id', '=', Session::get('market'))
                ->firstOrFail();

                dd($stall);

        return View::make('private.stall.edit')
                        ->with('stall', $stall);
    }

    
    /**
    * Ajax search
    *
    * @return json
    */
    public function ajaxSearch()
    {
        $stalls = Stall::has('activeOwner', 0)
                ->where('market_id', Session::get('market'))
                ->where('num', 'like', '%' . Input::get('term') . '%')
                ->with('sector')
                ->with('authprod')
                ->get();
                        
        return Response::json($stalls);
    }

}

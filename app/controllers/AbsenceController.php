<?php

class AbsenceController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $absences = Absence::whereHas('OwnerStall', function($q)
                {
                    $q->whereHas('Stall', function($q)
                    {
                        $q->where('market_id', Session::get('market'));
                    });
                })
                ->with('ownerstall', 'ownerstall.owner', 'ownerstall.stall')
                ->get();
     
        return View::make('private.absence.index')
                ->with('absences', $absences);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $ownerstall = OwnerStall::whereNull('deactivated_at')
                ->whereHas('stall', function($q)
                {
                    $q->where('market_id', '=', Session::get('market'));
                })
                ->find(Input::get('ownerstall_id'));

        return View::make('private.absence.create')
                        ->with('ownerstall', $ownerstall);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $nice = ['ownerstall_id' => 'titular',
                'document' => 'document'];
        
        //check for duplicate presence values
        $rules = [  'ownerstall_id' => 'required|validOwnerStall', //check belongs current market and is active
                    'document' => 'mimes:pdf'];
                
        if (Input::get('start_at') !=  Input::get('end_at'))
        {
            $rules['start_at'] = 'required|date|before:end_at'; //check absence is already on db
            $rules['end_at'] = 'required|date|after:start_at';
        }
        else
        {
            $rules['start_at'] = 'required|date'; //check absence is already on db
            $rules['end_at'] = 'required|date';
        }

        
        $inputs = Input::all();
        $inputs['document'] = Input::file('document');
        
        $validation = Validator::make($inputs, $rules);
        $validation->setAttributeNames($nice);

        if ($validation->fails())
        {
            return Redirect::route('absence.create', ['ownerstall_id' => Input::get('ownerstall_id')])
                            ->withErrors($validation)
                            ->withInput();
        }
        else
        {
            $absence = new Absence(Input::all());
            
            if (Input::hasFile('document') && Input::file('document')->isValid())
            {
                $file = Input::file('document');
                $absence->document = strtolower('absence-' . date('ymdhis') . '-' . $file->getClientOriginalName());
                $file->move("docs", $absence->document);
            }
            
            $absence->save();
            
            return Redirect::route('absence.index')
                            ->with(array('success_msg' => 'Absència afegida correctament'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}

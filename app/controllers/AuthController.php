<?php

class AuthController extends BaseController {

    public function getLogIn()
    {
        if (Auth::check())
            return Redirect::to('/');

        return View::make('public.login');
    }

    public function postLogIn()
    {
        $credentials = array(
            'dni' => Input::get('dni'),
            'password'=> Input::get('password')
        );

        if (Auth::validate($credentials))
        {
            $user = User::where('dni', '=', Input::get('dni'))->first();
            $market = $user->markets->first();
            
            if (!is_null($market))
            {
                Auth::login($user);
                Session::put('market', $market->id);
                
                return Redirect::to('/');
            }
            else
            {
                return Redirect::to('login')
                    ->with('error_msg', 'No tienes cap mercat assignat')
                    ->withInput();
            }
        }

        return Redirect::to('login')
                    ->with('error_msg', 'Les dades no són correctes')
                    ->withInput();
    }
    
    public function postLogOut()
    {
        Auth::logout();
        return Redirect::to('login')
                    ->with('error_msg', 'Has tancat la sessió.');
    }
    
    public function move($id)
    {
        if (!is_null(Auth::User()->markets->find($id)))
            Session::put('market', $id);
        
        return Redirect::back();
    }
}

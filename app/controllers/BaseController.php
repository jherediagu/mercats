<?php

class BaseController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

    public function __construct()
    {
        $role = Auth::check() ? Auth::user()->role : NULL;
        $menu = Array();
        if ($role == 'admin')
        {
            $menu[] = ['route' => 'user.index', 'text' => 'Usuaris'];
            $menu[] = ['route' => 'owner.index', 'text' => 'Titulars'];
            $menu[] = ['route' => 'stall.index', 'text' => 'Parades'];
            $menu[] = ['route' => 'sector.index', 'text' => 'Sectors'];
            $menu[] = ['route' => 'authprod.index', 'text' => 'Productes Autoritzats'];
            $menu[] = ['route' => 'tariff.index', 'text' => 'Tarifes'];
            $menu[] = ['route' => 'calendar.index', 'text' => 'Calendari'];
            $menu[] = ['route' => 'invoice.index', 'text' => 'Rebuts'];
            $menu[] = ['route' => 'absence.index', 'text' => 'Absències justificades'];
            $menu[] = ['route' => 'presence.index', 'text' => 'Absències no justificades'];
            $menu[] = ['route' => 'notes.index', 'text' => 'Incidències'];
            $menu[] = ['route' => 'ownerstall.index', 'text' => 'Històric'];
        }
        else if ($role == 'int-tab' || $role == 'int')
        {
            $menu[] = ['route' => 'owner.index', 'text' => 'Titulars'];
            $menu[] = ['route' => 'stall.index', 'text' => 'Parades'];
            $menu[] = ['route' => 'sector.index', 'text' => 'Sectors'];
            $menu[] = ['route' => 'authprod.index', 'text' => 'Productes Autoritzats'];
            $menu[] = ['route' => 'tariff.index', 'text' => 'Tarifes'];
            $menu[] = ['route' => 'calendar.index', 'text' => 'Calendari'];
            $menu[] = ['route' => 'invoice.index', 'text' => 'Rebuts'];
            $menu[] = ['route' => 'absence.index', 'text' => 'Absències justificades'];
            $menu[] = ['route' => 'presence.index', 'text' => 'Absències no justificades'];
            $menu[] = ['route' => 'notes.index', 'text' => 'Incidències'];
            $menu[] = ['route' => 'ownerstall.index', 'text' => 'Històric'];
        }
        else if ($role == 'tab')
        {
            $menu[] = ['route' => 'owner.index', 'text' => 'Titulars'];
            $menu[] = ['route' => 'stall.index', 'text' => 'Parades'];
            $menu[] = ['route' => 'invoice.index', 'text' => 'Rebuts'];
            $menu[] = ['route' => 'presence.index', 'text' => 'Absències no justificades'];
            $menu[] = ['route' => 'notes.index', 'text' => 'Incidències'];
        }
        
        
        View::share('menu', $menu);
        View::share('cMarket', Market::find(Session::get('market')));
    }

}

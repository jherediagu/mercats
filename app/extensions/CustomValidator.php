<?php

class CustomValidator extends Illuminate\Validation\Validator
{

    protected $implicitRules = array('Required', 'RequiredWith', 'RequiredWithout', 'RequiredIf', 'Accepted', 'RequiredWithoutField');

    public function __construct(\Symfony\Component\Translation\TranslatorInterface $translator, $data, $rules, $messages = array())
    {
        parent::__construct($translator, $data, $rules, $messages);
        $this->isImplicit('fail');
    }

    public function validateUniqueComposite($attribute, $value, $parameters = null)
    {
        return  (DB::table($parameters[0])
                    ->where($parameters[1], $value)
                    ->where($parameters[2], $parameters[3])
                    ->count() == 0);
    }
    
    public function validateValidOwnerStall($attribute, $value, $parameters = null)
    {
         $os = OwnerStall::whereHas('stall', function($q){
                            $q->where('market_id', '=', Session::get('market'));
                        })
                        ->find($value);
        
        if (is_null($os)) return FALSE; //stop
        
        return true;
    }
    
    public function validateValidPresence($attribute, $value, $parameters = null)
    {
        $os = OwnerStall::whereHas('stall', function($q){
                            $q->where('market_id', '=', Session::get('market'));
                        })
                        ->find($parameters[0]);
        
        if (is_null($os)) return FALSE; //stop
        
        $start = strtotime($os->discharge_date);
        $end = is_null($os->deactivated_at) ? NULL : strtotime($os->deactivated_at);
        $date = strtotime($value);
        
        if (!(($date >= $start && $date <= $end && !is_null($end)) || ($date >= $start && is_null($end))))
            return FALSE; // not active at current date
        
        $day = Calendar::where('date', 'like', date('Y-m-d', $date) . "%")
                    ->where('market_id', '=', Session::get('market'))
                    ->first();
        
        if (is_null($day)) return FALSE; // date not exists
        
        return (Presence::where('ownerstall_id', $os->id)->where('calendar_id', $day->id)->count() == 0); // check already exists
    }
}


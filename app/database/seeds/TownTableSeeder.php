<?php

class TownTableSeeder extends Seeder {
    public function run(){
        
        DB::table('town')->insert(array(
            'name'  => 'Manlleu'
        ));
        
        DB::table('town')->insert(array(
            'name'  => 'Vic'
        ));
        
        DB::table('town')->insert(array(
            'name'  => 'Tona'
        ));
        
        DB::table('town')->insert(array(
            'name'  => 'Mall'
        ));
    }
}


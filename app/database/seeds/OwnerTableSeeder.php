<?php

class OwnerTableSeeder extends Seeder {
    public function run(){
        
        for ($i=0; $i<100; $i++)
        {
            Owner::create(array(
                'dni'  => rand(10000000, 99999999) . substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1),
                'email'     => 'email' . rand(99,9999) . '@mercats.cat',
                'name'=> 'Nombre apellidos ' . rand(1,99),
            ));
        }
    }
}


<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
                
                //$this->call('TownTableSeeder');
                
                //$this->call('MarketTableSeeder');
                
                //$this->call('StallTableSeeder');
                
                //$this->call('OwnerTableSeeder');
                
                //$this->call('OwnerStallTableSeeder');
	}

}

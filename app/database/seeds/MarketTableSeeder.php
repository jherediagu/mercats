<?php

class MarketTableSeeder extends Seeder {
    public function run(){
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat de la plaça',
            'dayofweek' => 2,
            'town_id' => 1
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat del carrer X ',
            'dayofweek' => 5,
            'town_id' => 1
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat del carrer X',
            'dayofweek' => 2,
            'town_id' => 2
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat de la plaça',
            'dayofweek' => 6,
            'town_id' => 3
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat artesanal',
            'dayofweek' => 2,
            'town_id' => 3
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat del carrer X',
            'dayofweek' => 4,
            'town_id' => 3
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat de segona mà',
            'dayofweek' => 5,
            'town_id' => 3
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat principal',
            'dayofweek' => 3,
            'town_id' => 4
        ));
        
        DB::table('market')->insert(array(
            'name'  => 'Mercat del carrer Y',
            'dayofweek' => 5,
            'town_id' => 4
        ));
        
        
    }
}


<?php

class OwnerStallTableSeeder extends Seeder {
    public function run(){
        
        for ($i=0; $i<100; $i++)
        {
            OwnerStall::create(array(
                'owner_id'  => rand(1, 125),
                'stall_id' => rand(1,50),
                'discharge_date' => rand(2000, 2015) . '-' . rand(1,12) . '-' . rand(1,30),
                'expire_date' => rand(2000, 2015) . '-' . rand(1,12) . '-' . rand(1,30),
            ));
        }
    }
}


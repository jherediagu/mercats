<?php

class UserTableSeeder extends Seeder {
    public function run(){
        User::create(array(
            'dni'  => '12341234A',
            'email'     => 'admin@admin.com',
            'name'=> 'Administrator',
            'surname' => 'Admin',
            'password' => Hash::make('12341234'),
            'role' => 'admin',
            'town_id' => 3
        ));
    }
}


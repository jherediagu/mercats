<?php

class StallTableSeeder extends Seeder {
    public function run(){
        
        for ($i=0; $i<50; $i++)
        {
            DB::table('stall')->insert(array(
                'num'  => rand(1000, 9999),
                'length' => rand(2, 15),
                'active' => 1,
                'market_id' => rand(4, 12),
                'sector_id' => rand(1, 3),
                'auth_prod_id' => rand(1, 3)
            ));
        }
        
        
        
        
    }
}


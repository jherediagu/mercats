<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar', function($table)
        {
            $table->increments('id');
            $table->integer('market_id')->unsigned();
            
            $table->timestamp('date');
            
            $table->timestamps();
            $table->softDeletes();

            $table->unique(array('date', 'market_id'));
            
            $table->foreign('market_id')->references('id')->on('market');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendar');
    }

}

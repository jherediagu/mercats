<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('notes', function($table)
            {
                $table->increments('id');
                
                $table->integer('calendar_id')->unsigned()->nullable();
                $table->integer('ownerstall_id')->unsigned()->nullable();
                $table->integer('market_id')->unsigned()->nullable();
                
                $table->string('title', 500);
                $table->text('description');
                
                $table->string('pictures', 5000);
                $table->string('sent_to', 5000);
                
                $table->dateTime('solved_at')->nullable();
                $table->dateTime('happens_at');
                $table->timestamps();
                $table->softDeletes();
            
                $table->foreign('calendar_id')->references('id')->on('calendar');
                $table->foreign('ownerstall_id')->references('id')->on('ownerstall');
                $table->foreign('market_id')->references('id')->on('market');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('notes');
	}

}

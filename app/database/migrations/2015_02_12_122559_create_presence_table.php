<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresenceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('presence', function($table)
            {
                $table->integer('calendar_id')->unsigned();
                $table->integer('ownerstall_id')->unsigned();

                $table->timestamps();
                $table->softDeletes();
            
                $table->primary(array('calendar_id', 'ownerstall_id'));

                $table->foreign('calendar_id')->references('id')->on('calendar');
                $table->foreign('ownerstall_id')->references('id')->on('ownerstall');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('presence');
	}

}

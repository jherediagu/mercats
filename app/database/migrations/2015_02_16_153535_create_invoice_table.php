<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function($table)
        {
            $table->increments('id');
            $table->integer('ownerstall_id')->unsigned();

            $table->string('num', 100);

            $table->dateTime('month');
            $table->integer('qty');
            $table->float('price');
            $table->float('total');
            
            $table->datetime('paid')->nullable();
            $table->boolean('claimed');
            

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ownerstall_id')->references('id')->on('ownerstall');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice');
    }

}

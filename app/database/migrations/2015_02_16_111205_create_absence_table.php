<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absence', function($table)
        {
            $table->increments('id');
            $table->integer('ownerstall_id')->unsigned();

            $table->string('document', 500);

            $table->dateTime('start_at');
            $table->dateTime('end_at');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ownerstall_id')->references('id')->on('ownerstall');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('absence');
    }

}

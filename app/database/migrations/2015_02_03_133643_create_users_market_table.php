<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersMarketTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_market', function($table)
        {
            $table->integer('user_id')->unsigned();
            $table->integer('market_id')->unsigned();

            $table->primary(array('user_id', 'market_id'));
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('market_id')->references('id')->on('market');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_market');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStallOwnerTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ownerstall', function($table)
        {
            $table->increments('id');
            $table->integer('stall_id')->unsigned();
            $table->integer('owner_id')->unsigned();
            $table->string('insurance', 200);
            
            $table->timestamp('discharge_date');
            $table->timestamp('expire_date');
            $table->timestamp('transfer_date');
            
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('stall_id')->references('id')->on('stall');
            $table->foreign('owner_id')->references('id')->on('owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ownerstall');
    }

}

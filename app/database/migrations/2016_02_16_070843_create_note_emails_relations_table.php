<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteEmailsRelationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('note_emails', function(Blueprint $table)
		{

			$table->increments('id');
			$table->bigInteger('id_email');
			$table->string('name',255);
			$table->string('email',255);

			// created_at & updated_at
			$table->timestamp('created_at');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('note_emails');
	}

}

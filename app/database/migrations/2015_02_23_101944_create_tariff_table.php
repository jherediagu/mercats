<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTariffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('tariff', function($table)
            {
                $table->increments('id');
                $table->integer('market_id')->unsigned();
                $table->string('name', 100);

                $table->float('price');

                $table->timestamps();
                $table->softDeletes();
                
                $table->foreign('market_id')->references('id')->on('market');
            });
            
            Schema::table('ownerstall', function($table)
            {
                $table->integer('tariff_id')->unsigned();
                $table->foreign('tariff_id')->references('id')->on('tariff');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tariff');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStallTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stall', function($table)
        {
            $table->increments('id');
            $table->string('num', 20)->unique();
            $table->integer('length');
            $table->boolean('active');
            
                    
            $table->integer('market_id')->unsigned();
            $table->integer('sector_id')->unsigned();
            $table->integer('auth_prod_id')->unsigned();
            
            $table->timestamps();
            $table->softDeletes();
            
            $table->unique(array('num', 'market_id'));
            $table->foreign('market_id')->references('id')->on('market');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stall');
    }

}

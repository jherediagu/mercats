<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner', function($table)
        {
            $table->increments('id');
            $table->string('dni', 20)->unique();
            $table->string('name', 500);
            $table->string('address', 500);
            $table->string('email', 100)->unique();
            $table->string('phone', 20);
            $table->string('iban', 100);
            $table->string('image', 500);
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('owner');
    }

}

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Administració</title>

        {{ HTML::style('assets/bs/css/bootstrap.min.css'); }}
        {{ HTML::style('assets/css/styles.css'); }}

        <style>
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #eee;
            }

            .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin .checkbox {
                font-weight: normal;
            }
            .form-signin .form-control {
                position: relative;
                height: auto;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                padding: 10px;
                font-size: 16px;
            }
            .form-signin .form-control:focus {
                z-index: 2;
            }
            .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
            .form-signin input[type="password"] {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
        </style>
    </head>

    <body>

        <div class="container">
            
            <div class="col-md-3"></div>
                <div class="col-md-6 admin-box text-center">
                             <a href="{{url('/')}}"><img class="img-home" src="{{url('/')}}/assets/images/mercalogo.png"></img></a>

                    {{ Form::open(array('url' => 'login', 'class' => 'form-signin')) }}
                    <h2 class="form-signin-heading">Administració</h2>

                    @if(Session::has('error_msg'))
                        <div class="alert alert-danger text-center" role="alert">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            {{ Session::get('error_msg') }}
                        </div>
                    @endif

                    {{ Form::text('dni', Input::old('dni'), Array('class' => 'form-control', 'placeholder' => 'Usuari', 'required', 'autofocus')); }}<br>
                    {{ Form::password('password', Array('class' => 'form-control', 'placeholder' => 'Contrasenya', 'required')); }}
                    <div class="checkbox">
                        <label>
                            {{ Form::checkbox('rememberme', true) }} Recorda'm
                        </label>
                    </div>
                    {{ Form::submit('Accedir', Array('class' => 'btn btn-lg btn-primary btn-block boton-color')) }}
                    {{ Form::close() }}
                    <br><br>

            </div>

            <div class="col-md-3"></div>

        </div> 

    </body>
</html>

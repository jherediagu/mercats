@if(Session::has('success_msg'))
<div class="alert alert-success text-center" role="alert">
    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
    {{ Session::get('success_msg') }}
</div>
@endif

@if(Session::has('error_msg'))
<div class="alert alert-danger text-center" role="alert">
    <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
    {{ Session::get('error_msg') }}
</div>
@endif


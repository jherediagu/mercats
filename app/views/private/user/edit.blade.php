@include('private.header')

{{ HTML::style('assets/bs-multiselect/dist/css/bootstrap-multiselect.css'); }}

<div class="panel panel-default">
    <div class="panel-heading">
        Editar usuari
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::model($user, array('route' => array('user.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            <label class="col-sm-3 control-label">Nom</label>
            <div class="col-sm-9">
                {{ Form::text('name', Input::old('name'), Array('class' => 'form-control', 'required', 'autofocus')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Cognoms</label>
            <div class="col-sm-9">
                {{ Form::text('surname', Input::old('surname'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Correu</label>
            <div class="col-sm-9">
                {{ Form::email('email', Input::old('email'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">D.N.I.</label>
            <div class="col-sm-9">
                {{ Form::text('dni', Input::old('dni'), Array('class' => 'form-control', 'required')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Mercats</label>
            <div class="col-sm-9">
                {{ Form::select('markets[]', $markets, $user->markets()->getRelatedIds(), ['multiple', 'class' => 'multiple']) }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Rol</label>
            <div class="col-sm-9">
                <div class="radio">
                    <label>
                        {{ Form::radio('role', 'int-tab', (Input::old('role') == 'int-tab'), ['required']) }}
                        Intranet + Tablet
                    </label>
                </div>
                <div class="radio">
                    <label>
                        {{ Form::radio('role', 'int', (Input::old('role') == 'int'), ['required']) }}
                        Intranet
                    </label>
                </div>
                <div class="radio">
                    <label>
                        {{ Form::radio('role', 'tab', (Input::old('role') == 'tab'), ['required']) }}
                        Tablet
                    </label>
                </div>
            </div>
        </div>

        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('user') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')


{{ HTML::script('assets/bs-multiselect/dist/js/bootstrap-multiselect.js'); }}

<script type="text/javascript">
    $(document).ready(function () {
        $('.multiple').multiselect({
            maxHeight: 250,
            buttonWidth: '100%',
            numberDisplayed: 4,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true
        });
    });
</script>

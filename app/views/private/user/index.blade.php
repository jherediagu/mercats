@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Usuaris
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('user.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        @if(Session::has('status'))
        <div class="alert alert-success text-center" role="alert">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            {{ Session::get('status') }}
        </div>
        @endif

        @if ($users->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha usuaris per aquesta ciutat
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Cognoms</th>
                    <th>Correu</th>
                    <th>Rol</th>
                    <th class="text-center">Opcions</th>
                </tr>
            </thead>
            @foreach ($users as $key => $value)
            <tr>
                <td>{{$value->name}}</td>
                <td>{{$value->surname}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->role}}</td>
                <td class="text-center">
                    <form action="{{ action('RemindersController@postRemind') }}" method="POST">
                        <input type="hidden" name="email" value="{{$value->email}}">
                        <button class="btn btn-default btn-xs confirm-reminder ttip" data-toggle="tooltip" data-placement="top" title="Canviar contrasenya" type="submit" value="Send Reminder">
                            <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                        </button>
                    </form>
                    
                    {{ Form::open(array('route' => ["user.destroy", $value->id])) }}
                    <a class='btn btn-default btn-xs ttip' href="{{ URL::route('user.edit', $value->id) }}" data-toggle="tooltip" data-placement="top" title="Editar usuari">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn btn-danger btn-xs confirm ttip', "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>"Eliminar usuari", 'type'=>'submit']) }}
                    {{ Form::close() }}
                    
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}
<style>
    form {display:inline;}
</style>
<script>
    $('.confirm').click(function () {
        if (confirm("Desitja eliminar l'usuari?") === false)
        {
            event.preventDefault();
            return false;
        }
    });
    
    $('.confirm-reminder').click(function () {
        if (confirm("Desitja enviar un correu a l'usuari per canviar la contrasenya?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    $(document).ready(function () {
        $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            columnDefs: [{orderable: false, targets: [4]}],
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
    });
</script>
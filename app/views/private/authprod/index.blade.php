@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Productes autoritzats
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('authprod.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        @if ($authprods->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha productes autoritzats
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th class="text-center">Editar</th>
                </tr>
            </thead>
            @foreach ($authprods as $key => $value)
            <tr>
                <td>{{$value->name}}</td>
                <td class="text-center">
                    {{ Form::open(array('url' => "authprod/{$value->id}")) }}
                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Editar producte" href="{{ URL::route('authprod.edit', $value->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Veure fitxa" href="{{ URL::route('authprod.show', $value->id) }}">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    </a>
                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Parades Asignades" href="{{ URL::route('authprod.showsector', $value->id) }}">
                        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                    </a>
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn  btn-xs btn-danger confirm', 'data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"Eliminar producte", 'type'=>'submit']) }}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
    $('.confirm').click(function () {
        if (confirm("Desitja eliminar el producte autoritzat?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    $(document).ready(function () {
        $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            columnDefs: [{orderable: false, targets: [1]}],
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
    });
</script>
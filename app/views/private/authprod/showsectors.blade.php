@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}


<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Parades que inclouen el producte autoritzat : <b>{{$authprod->name}}</b>
            </div>
            <div class="col-md-4 text-right">
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Numero de parada</th>
                    <th class="text-center">Veure parada</th>
                </tr>
            </thead>
            @foreach ($stalls as $stall => $sector)
                
                @foreach ($rel_prod as $key => $value)
   
                    @if ($value->id_sector == $sector)   
                        <tr>
                            <td>Nº de parada : {{$stall}}</td>
                            <td class="text-center">
                                <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Veure fitxa" href="{{ URL::route('stall.show', $stall) }}">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                </a>
                            
                            </td>
                        </tr>
                    @endif
                
                @endforeach

            @endforeach
        </table>
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
    $('.confirm').click(function () {
        if (confirm("Desitja eliminar el producte autoritzat?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    $(document).ready(function () {
        $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            columnDefs: [{orderable: false, targets: [1]}],
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
    });
</script>
@if($errors->has())
<div class="alert alert-danger text-center" role="alert">
    <p>
        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
        Corregeix els següents errors per continuar:
    </p>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif


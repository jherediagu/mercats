@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Tarifes
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('tariff.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir tarifa
                </a>
            </div>
        </div>
    </div>
    
    <div class="panel-body">
        @include('private.success')

        @if ($tariff->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha tarifes
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th class="text-right">Preu</th>
                    <th class="text-center">Editar</th>
                </tr>
            </thead>
            @foreach ($tariff as $key => $value)
            <tr>
                <td>{{$value->name}}</td>
                <td class="text-right">{{$value->price}} €/m</td>
                <td class="text-center">
                    <a class='btn btn-default btn-xs' href="{{ URL::route('tariff.edit', $value->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<style>
    #datatable
    {
        margin-top: 20px !important;
        margin-bottom: 40px !important;
    }
</style>
<script>
    $(document).ready(function () {
        $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            columnDefs: [{orderable: false, targets: [2]}],
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
    });
</script>
@include('private.header')

<div class="panel panel-default">
    <div class="panel-heading">
        Editar tarifa
    </div>
    <div class="panel-body">

        @if($errors->has())
        <div class="alert alert-danger text-center" role="alert">
            <p>
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                Debe corregir los siguientes errores
            </p>
            @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
        </div>
        @endif

        {{ Form::model($tariff, array('route' => array('tariff.update', $tariff->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            <label class="col-sm-3 control-label">Nombre</label>
            <div class="col-sm-9">
                {{ Form::text('name', Input::old('name'), Array('class' => 'form-control', 'required', 'autofocus')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Preu</label>
            <div class="col-sm-9">
                {{ Form::text('price', Input::old('price'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('tariff') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')


@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Incidències
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('notes.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        @if ($notes->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha incidències
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Parada</th>
                    <th>Titular</th>
                    <th>Titol</th>
                    <th>Resolta?</th>
                    <th class="text-center">Opcions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Data</th>
                    <th>Parada</th>
                    <th>Titular</th>
                    <th>Titol</th>
                    <th>Resolta?</th>
                    <th></th>
                </tr>
            </tfoot>
            @foreach ($notes as $value)
            <tr>
                <td data-order="{{strtotime($value->happens_at)}}">{{strftime("%d %b %Y", strtotime($value->happens_at))}}</td>
                <td>
                    @if (is_null($value->ownerstall))
                        General
                    @else
                    <a href="{{URL::route('stall.show', $value->ownerstall->stall->id)}}">
                        {{ $value->ownerstall->stall->num }}
                    </a>
                    @endif
                <td>
                    @if (is_null($value->ownerstall))
                        General
                    @else
                    <a href="{{URL::route('owner.show', $value->ownerstall->owner->id)}}">
                        {{ $value->ownerstall->owner->dni }} - {{$value->ownerstall->owner->name}}
                    </a>
                    @endif
                <td>{{$value->title}}</td>
                @if (is_null($value->solved_at))
                    <td class="text-center warning">No</td>
                @else
                    <td class="text-center success">{{strftime('%d %b %Y', strtotime($value->solved_at))}}</td>
                @endif
                
                    
                <td class="text-center">
                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Veure fitxa" href="{{ URL::route('notes.show', $value->id) }}">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    </a>
                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Editar incidència" href="{{ URL::route('notes.edit', $value->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                    {{ Form::open(array('url' => "notes/{$value->id}")) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn  btn-xs btn-danger confirm', 'data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"Eliminar producte", 'type'=>'submit']) }}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
        
    $(document).ready(function () {
        $('#datatable tfoot th').each(function () {
            var title = $('#datatable thead th').eq($(this).index()).text();
            $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
        });
        
        var table = $('#datatable').DataTable({
            dom: "lTfrtip",
            language: spanish,
            columnDefs: [{orderable: false, targets: [5]}],
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
        
        // Apply the search
        table.columns().eq(0).each(function (colIdx) {
            $('input', table.column(colIdx).footer()).on('keyup change', function () {
                table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
            });
        });
    });
</script>
@include('private.header')


{{ HTML::style('assets/jquery-ui/jquery-ui.css'); }}

{{ HTML::style('assets/jquery-ui/bs-theme/jquery.ui.theme.css'); }}

{{ HTML::style('assets/datepicker/dist/css/bootstrap-datepicker.css') }}

{{ HTML::style('assets/fileinput/css/fileinput.css'); }}

<div class="panel panel-default">
    <div class="panel-heading">
        Editar incidència
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::model($note, array('route' => array('notes.update', $note->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            <label class="col-sm-3 control-label">Tipus</label>
            <div class="col-sm-9">
                <div class="radio">
                    <label>
                        {{ Form::radio('type', 0, !is_null($note->ownerstall), ['class' => 'radio-type']) }}
                        Incidència per parada/titular
                    </label>
                </div>
                <div class="radio">
                    <label>
                        {{ Form::radio('type', 1, is_null($note->ownerstall), ['class' => 'radio-type']) }}
                        Incidència de tipus general
                    </label>
                </div>
            </div>
        </div>
        <fieldset id="fieldset-search">
            <div class="form-group">
                <label class="col-sm-3 control-label">Buscar</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group ui-widget">
                                {{ Form::text('search1', null, Array('id' => 'inp-search-owner', 'autofocus', 'class' => 'form-control', 'placeholder' => 'Buscar titular')); }}
                                <span class="input-group-addon" id="basic-addon2">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group ui-widget">
                                {{ Form::text('search2', null, Array('id' => 'inp-search-stall', 'class' => 'form-control', 'placeholder' => 'Buscar parada')); }}
                                <span class="input-group-addon" id="basic-addon2">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="div-owner" <?= (is_null($note->ownerstall)) ? 'class="collapse"' : '' ?>>
                {{ Form::hidden('ownerstall_id', (!is_null($note->ownerstall)) ? $note->ownerstall->id : null, ['id' => 'hidden-ownerstall']) }}
                <div class="form-group">
                    <label class="col-sm-3 control-label">D.N.I.</label>
                    <div class="col-sm-9">
                        <p id="dni" class="form-control-static">
                            {{ (!is_null($note->ownerstall)) ? $note->ownerstall->owner->dni : '' }}
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nombre</label>
                    <div class="col-sm-9">
                        <p id="name" class="form-control-static">
                            {{ (!is_null($note->ownerstall)) ? $note->ownerstall->owner->name : '' }}
                        </p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Parada</label>
                    <div class="col-sm-9">
                        <p id="num" class="form-control-static">
                            {{ (!is_null($note->ownerstall)) ? $note->ownerstall->stall->num : '' }}
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Longitud</label>
                    <div class="col-sm-9">
                        <p id="length" class="form-control-static">
                            {{ (!is_null($note->ownerstall)) ? "{$note->ownerstall->stall->length}m" : '' }}
                        </p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha de alta</label>
                    <div class="col-sm-9">
                        <p id="discharge_date" class="form-control-static">
                            {{ (!is_null($note->ownerstall)) ? $note->ownerstall->discharge_date : '' }}
                        </p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha de caducidad</label>
                    <div class="col-sm-9">
                        <p id="expire_date" class="form-control-static">
                            {{ (!is_null($note->ownerstall)) ? $note->ownerstall->expire_date : '' }}
                        </p>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="form-group" style="margin-top:15px;">
            <label class="col-sm-3 control-label">Data</label>
            <div class="col-sm-9">
                {{ Form::text('happens_at', date('d-m-Y', strtotime($note->happens_at)), Array('id' => 'date', 'class' => 'form-control', 'required')); }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Títol</label>
            <div class="col-sm-9">
                {{ Form::text('title', Input::old('title'), Array('class' => 'form-control', 'required')); }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Descripció</label>
            <div class="col-sm-9">
                {{ Form::textarea('description', Input::old('description'), Array('class' => 'form-control', 'required')); }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Enviar</label>
            <div class="col-sm-9">
                {{ Form::text('sent_to', Input::old('sent_to'), Array('class' => 'form-control')); }}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Resolta</label>
            <div class="col-sm-9">
                {{ Form::text('solved_at', is_null($note->solved_at) ? null : date('d-m-Y', strtotime($note->solved_at)), Array('id' => 'date1', 'class' => 'form-control', 'required')); }}
                @if (is_null($note->solved_at))
                <p class="help-block">Incidència no resolta.</p>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Image</label>
            <div class="col-sm-9">
                @if ($note->images != '')
                {{ HTML::image('imgs/notes/' . $note->images, null, array('class' => 'img-responsive', 'style' => 'height:200px;margin-top:20px;')) }}
                @endif
                <input type="file" name="images" accept="image/*" capture>
                <span id="helpBlock" class="help-block">Utilitzi aquest camp per canviar l'imatge.</span>
            </div>
        </div>

        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('notes') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel·lar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}



    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/jquery-ui/jquery-ui.js'); }}
{{ HTML::script('assets/datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}
{{ HTML::script('assets/fileinput/js/fileinput.min.js'); }}

<style>
    .available
    {
        color: green;
        font-weight: bold;
    }
    fieldset:disabled 
    {
        opacity:0.6;
    }
</style>
<script type="text/javascript">
    function available(date)
    {
        dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        if ($.inArray(dmy, availableDates) != -1) {
            return {enabled: true, classes: 'available'};
        } else {
            return {enabled: true};
        }
    }

    var availableDates = [];
<?php foreach ($calendar as $value): ?>
        availableDates.push('{{date("j-n-Y", strtotime($value->date))}}');
<?php endforeach; ?>


    function fill(ui)
    {
        $('#hidden-ownerstall').val(ui.item.id);
        $('#dni').html(ui.item.owner.dni);
        $('#name').html(ui.item.owner.name);

        $('#num').html(ui.item.stall.num);
        $('#length').html(ui.item.stall.length + 'm');

        $('#discharge_date').html(ui.item.discharge_date);
        $('#expire_date').html(ui.item.expire_date);

        $('#div-owner').collapse('show');
    }


    $('.radio-type').change(function () {
        $('#fieldset-search').prop('disabled', $(this).val() == 1);
    });

    $(document).ready(function () {

        $("#images").fileinput({
            allowedFileTypes: ['image'],
            showUpload: false,
            maxFileCount: 3,
            overwriteInitial: false,
            initialPreview: [
                "<img id='img-1' src='' class='file-preview-image'>",
                "<img src='' class='file-preview-image'>"
            ]
        });

        $('#fieldset-search').prop('disabled', $('.radio-type:checked').val() == 1);

        $('#inp-search-owner').autocomplete({
            source: "{{ URL::to('ownerstall/ajaxOwnerSearch') }}",
            minLength: 2,
            select: function (event, ui) {
                fill(ui);
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                    .append("<b>" + item.stall.num + "</b>: " + item.owner.dni + " - " + item.owner.name)
                    .appendTo(ul);
        };


        $('#inp-search-stall').autocomplete({
            source: "{{ URL::to('ownerstall/ajaxStallSearch') }}",
            minLength: 2,
            select: function (event, ui) {
                fill(ui);
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                    .append("<b>" + item.stall.num + "</b>: " + item.owner.dni + " - " + item.owner.name)
                    .appendTo(ul);
        };

        $('#date, #date1').datepicker({
            format: 'dd-mm-yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            beforeShowDay: function (dt) {
                return available(dt);
            },
            changeMonth: true,
            changeYear: false
        });
    });
</script>



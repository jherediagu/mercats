@include('private.header')

<div class="panel panel-default">
    <div class="panel-heading">
        Fitxa d'incidència
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                @if ($note->images != '')
                {{ HTML::image('imgs/notes/' . $note->images, null, array('class' => 'img-responsive')) }}
                @endif
            </div>
            <div class="col-md-9 form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Creació</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{strftime('%d %B %Y %H:%M', strtotime($note->created_at))}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Data</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{strftime('%d %B %Y', strtotime($note->happens_at))}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Resolta</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            @if (is_null($note->solved_at))
                            No resolta
                            @else
                            {{strftime('%d %B %Y', strtotime($note->solved_at))}}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Titular</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            @if (is_null($note->ownerstall))
                                General
                            @else
                            <a href="{{URL::route('owner.show', $note->ownerstall->owner->id)}}">
                                {{ $note->ownerstall->owner->dni }} - {{$note->ownerstall->owner->name}}
                            </a>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Parada</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            @if (is_null($note->ownerstall))
                                General
                            @else
                            <a href="{{URL::route('stall.show', $note->ownerstall->stall->id)}}">
                                {{ $note->ownerstall->stall->num }}
                            </a>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Descripció</label>
                    <div class="col-sm-10">
                        <p class="form-control-static text-justify">{{nl2br($note->description)}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Enviat a</label>
                    <div class="col-sm-10">
                        
                            @foreach((array)explode(';', $note->sent_to) as $email)
                                <p class="form-control-static">{{$email}}</p>
                            @endforeach
                        
                    </div>
                </div>
                

            </div>
        </div>

    </div>

</div>

@include('private.footer')
@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datepicker/dist/css/bootstrap-datepicker.css') }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}


<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Parades
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('ownerstall.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Assignar parada
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        @if ($stalls->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha parades
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Parada</th>
                    <th class="text-right">Metres</th>
                    <th>Producte</th>
                    <th>Sector</th>
                    <th>Titular</th>
                    <th>Alta</th>
                    <th>Caducitat</th>
                    <th class="text-center">Activa</th>
                    <th class="text-center">Opcions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td id="col-0"></td>
                    <td id="col-1" class="text-center"></td>
                    <td id="col-2"></td>
                    <td id="col-3" ></td>
                    <td id="col-4"></td>
                    <td id="col-5"></td>
                    <td id="col-6"></td>
                    <td id="col-7" class="text-center"></td>
                    <td id="col-8"></td>
                </tr>
            </tfoot>
            @foreach ($stalls as $value)
            <tr> 
                <td><a href="{{ URL::route('stall.show', $value->id) }}">{{$value->num}}</a></td>
                <td class="text-right">{{$value->length}}</td>
                <td>{{$value->authprod->name}}</td>
                <td>{{$value->sector->name}}</td>
                @if ($value->activeOwner->isEmpty())
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-center warning">No</td>
                    <td class="text-center">
                        <a class='btn btn-xs btn-default ttip' data-toggle="tooltip" data-placement="top" title="Assignar titular" href="{{ URL::route('ownerstall.create', ['stall_id' => $value->id]) }}">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </a>
                    </td>
                @else
                    <?php $activeOwner = $value->activeOwner->first(); ?>
                    <td><a href="{{ URL::route('owner.show', $activeOwner->id) }}">{{$activeOwner->dni}} {{$activeOwner->name}}</a></td>
                    <td>{{date('d/m/Y', strtotime($activeOwner->pivot->discharge_date))}}</td>
                    <td>{{date('d/m/Y', strtotime($activeOwner->pivot->expire_date))}}</td>
                    
                    <td class="text-center success">Si</td>
                    <td class="text-center">
                        {{ Form::open(array('url' => "ownerstall/{$activeOwner->pivot->id}")) }}
                        <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Editar dates" href="{{ URL::route('ownerstall.edit', $activeOwner->pivot->id) }}">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </a>
                        {{ Form::hidden('_method', 'DELETE') }}

                        <? $date_now = date('Y-m-d H:i:s'); ?>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Donar de Baixa</h4>
                              </div>
                              <div class="modal-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Data de baixa</label>
                                        <div class="col-sm-9">
                                            {{ Form::input('date', 'deactivated_at', date('Y-m-d', strtotime($date_now)), Array('class' => 'form-control')) }}
                                        </div>
                                    </div>
                              </div>
                              <div class="modal-footer">
                                <div class="text-right">
                                    <a data-dismiss="modal" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel·lar</a>
                                    {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Donar de Baixa', ['class'=>'btn btn-danger', 'title'=>"Donar de baixa", 'type'=>'submit']) }}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Modal -->

                        {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn  btn-xs btn-danger confirm ttip', 'data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"Donar de baixa", 'data-toggle'=>"modal" , 'data-target'=>"#myModal"]) }}
                        {{ Form::close() }}
                    </td>
                @endif
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}
{{ HTML::script('assets/moment.min.js')}}

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
    $('.confirm').click(function () {
        if (confirm("Desitja donar de baixa el titular?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    function count(api, column, type)
    {
        var result = {active: 0, inactive: 0};

        api.column(column, type).data().each(function (v) {
            if (v == 'Si')
                result.active += 1;
            else if (v == 'No')
                result.inactive += 1;
        });
        return result;
    }
    
    function sum(api, column, search, type)
    {
        var result = {active: 0.0, inactive: 0.0};

        var column = api.column(column, type).data();
        var search = api.column(search, type).data();
        
        column.each(function(v, k){
            if (search[k] == 'Si')
                result.active += parseFloat(v);
            else if (search[k] == 'No')
                result.inactive += parseFloat(v);
        });
        return result;
    }
    
    $('#datatable tfoot th').each(function () {
        var title = $('#datatable thead th').eq($(this).index()).text();
        $(this).html('<input id="'+title+'"type="text" class="form-control" placeholder="' + title + '" />');
    });

    $(document).ready(function () {

        $('#Caducitat').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });


        $('#Alta').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });


        
        var table = $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            },
            columnDefs: [{orderable: false, targets: [8]}],
            footerCallback: function (row, data, start, end, display) {

                var api = this.api(), data;

                //var total = count(api, 7, {});
                //var current = count(api, 7, {page: 'current'});
                var filter = count(api, 7, {filter: 'applied'});

                $('#col-7').html(
//                        'Si (pagina): ' + current.active + '<br>' +
                        'Si: ' + filter.active + '<br>' +
//                        'Si (total): ' + total.active + '<hr>' +
//                        'No (pagina): ' + current.inactive + '<br>' +
                        'No: ' + filter.inactive + '<br>'
//                        + 'No (totals): ' + total.inactive + '<hr>'
                        );
                
                //var total = sum(api, 1, 7, {});
                //var current = sum(api, 1, 7, {page: 'current'});
                var filter = sum(api, 1, 7, {filter: 'applied'});
                
                $('#col-1').html(
//                        'Si (pagina): ' + current.active + 'm<br>' +
                        'Actius: ' + filter.active + 'm<br>' +
//                        'Si (total): ' + total.active + 'm<hr>' +
//                        'No (pagina): ' + current.inactive + 'm<br>' +
                        'Inactius: ' + filter.inactive + 'm<br>'
//                         +'No (totals): ' + total.inactive + 'm<hr>'
                );

            }
        });
        
        // Apply the search
        table.columns().eq(0).each(function (colIdx) {
            $('input', table.column(colIdx).footer()).on('keyup change', function () {
                table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
            });
        });
    });
</script>
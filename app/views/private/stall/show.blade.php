@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}

<div class="panel panel-default">
    <div class="panel-heading">
        Fitxa de la parada <b>{{ $stall->num }}</b>
                <a class="btn btn-default btn-md ttip confirm-print" style="padding-left: 20px;padding-right:20px;margin-left: 30px" href="{{ $stall->id }}/edit" data-toggle="tooltip" data-placement="top" title="editar">
                      Editar les dades de la parada                  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
               
    </div>
    <div class="panel-body">

        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Número</label>
                <div class="col-sm-9">
                    <p class="form-control-static">#{{ $stall->num }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Longitut</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $stall->length }}m</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Producte autoritzat</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $stall->authprod->name }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Sector</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $stall->sector->name }}</p>
                </div>
            </div>
                       

            <hr>

            <div role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">Històric</a></li>
                    <li role="presentation"><a href="#presence" aria-controls="presence" role="tab" data-toggle="tab">Absències no justificada</a></li>
                    <li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Incidències</a></li>
                    <li role="presentation"><a href="#absences" aria-controls="absence" role="tab" data-toggle="tab">Absències</a></li>
                    <li role="presentation"><a href="#invoice" aria-controls="invoice" role="tab" data-toggle="tab">Rebuts</a></li>
                </ul>

                <div class="tab-content"  style='margin-top:20px;'>

                    <!-- HISTORIC -->
                    <div role="tabpanel" class="tab-pane active" id="history">
                        <table id="datatable" class="table table-striped table-hover datatable">
                            <thead>
                                <tr>
                                    <th>Titular</th>
                                    <th>Data d'alta</th>
                                    <th>Data de caducitat</th>
                                    <th class="text-center">Estat</th>
                                </tr>
                            </thead>
                            @foreach ($stall->owners as $value)
                            <tr>
                                <td><a href="{{ URL::route('owner.show', $value->id) }}">{{$value->dni}} - {{$value->name}}</a></td>
                                <td>{{$value->pivot->discharge_date}}</td>
                                <td>{{$value->pivot->expire_date}}</td>
                                <td class="text-center">
                                    @if (is_null($value->pivot->deactivated_at))
                                    <span class="label label-success">Parada activada el {{$value->pivot->created_at}} </span>
                                    @else
                                    <span class="label label-warning">Desactivada el {{$value->pivot->deactivated_at}}</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>

                    <!-- PRESENCE -->
                    <div role="tabpanel" class="tab-pane" id="presence">
                        <table id="datatable1" class="table table-striped table-hover datatable">
                            <thead>
                                <tr>
                                    <th>Titular</th>
                                    <th>Data</th>
                                </tr>
                            </thead>
                            @foreach ($presence as $value)
                            <tr>
                                <td><a href="{{ URL::route('owner.show', $value->ownerstall->owner->id) }}">{{$value->ownerstall->owner->dni}} - {{$value->ownerstall->owner->name}}</a></td>
                                <td>{{$value->calendar->date}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>

                    <!-- NOTES -->
                    <div role="tabpanel" class="tab-pane" id="notes">
                        <table id="datatable" class="table table-striped table-hover datatable">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Parada</th>
                                    <th>Titular</th>
                                    <th>Titol</th>
                                    <th>Resolta?</th>
                                    <th class="text-center">Opcions</th>
                                </tr>
                            </thead>
                            <!--@foreach ($general as $value)
                            <tr>
                                <td data-order="{{strtotime($value->happens_at)}}">{{strftime("%d %b %Y", strtotime($value->happens_at))}}</td>
                                <td>
                                    @if (is_null($value->ownerstall))
                                    General
                                    @else
                                    <a href="{{URL::route('stall.show', $value->ownerstall->stall->id)}}">
                                        {{ $value->ownerstall->stall->num }}
                                    </a>
                                    @endif
                                <td>
                                    @if (is_null($value->ownerstall))
                                    General
                                    @else
                                    <a href="{{URL::route('owner.show', $value->ownerstall->owner->id)}}">
                                        {{ $value->ownerstall->owner->dni }} - {{$value->ownerstall->owner->name}}
                                    </a>
                                    @endif
                                <td>{{$value->title}}</td>
                                @if (is_null($value->solved_at))
                                <td class="text-center warning">No</td>
                                @else
                                <td class="text-center success">{{strftime('%d %b %Y', strtotime($value->solved_at))}}</td>
                                @endif


                                <td class="text-center">
                                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Veure fitxa" href="{{ URL::route('notes.show', $value->id) }}">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Editar incidència" href="{{ URL::route('notes.edit', $value->id) }}">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach-->
                            @foreach ($notes as $value)
                            <tr>
                                <td data-order="{{strtotime($value->happens_at)}}">{{strftime("%d %b %Y", strtotime($value->happens_at))}}</td>
                                <td>
                                    @if (is_null($value->ownerstall))
                                    General
                                    @else
                                    <a href="{{URL::route('stall.show', $value->ownerstall->stall->id)}}">
                                        {{ $value->ownerstall->stall->num }}
                                    </a>
                                    @endif
                                <td>
                                    @if (is_null($value->ownerstall))
                                    General
                                    @else
                                    <a href="{{URL::route('owner.show', $value->ownerstall->owner->id)}}">
                                        {{ $value->ownerstall->owner->dni }} - {{$value->ownerstall->owner->name}}
                                    </a>
                                    @endif
                                <td>{{$value->title}}</td>
                                @if (is_null($value->solved_at))
                                <td class="text-center warning">No</td>
                                @else
                                <td class="text-center success">{{strftime('%d %b %Y', strtotime($value->solved_at))}}</td>
                                @endif


                                <td class="text-center">
                                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Veure fitxa" href="{{ URL::route('notes.show', $value->id) }}">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Editar incidència" href="{{ URL::route('notes.edit', $value->id) }}">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>

                    <!-- ABSENCES -->
                    <div role="tabpanel" class="tab-pane" id="absences">
                        <table id="datatable" class="table table-striped table-hover datatable">
                            <thead>
                                <tr>
                                    <th>Titular</th>
                                    <th>Data inici</th>
                                    <th>Data de fi</th>
                                    <th>Document</th>
                                </tr>
                            </thead>
                            @foreach ($absences as $value)
                            <tr>
                                <td><a href="{{ URL::route('owner.show', $value->ownerstall->owner->id) }}">{{$value->ownerstall->owner->dni}} - {{$value->ownerstall->owner->name}}</a></td>
                                <td>{{date('d-m-Y', strtotime($value->start_at))}}</td>
                                <td>{{date('d-m-Y', strtotime($value->end_at))}}</td>
                                <td>
                                    @if($value->document != '')
                                    <a href='{{url('docs/' . $value->document)}}' target='_blank'>Veure</a>
                                    @else
                                    -
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>

                    <!-- INVOICE -->
                    <div role="tabpanel" class="tab-pane" id="invoice">
                        <table id="datatable" class="table table-striped table-hover table-condensed datatable">
                            <thead>
                                <tr>
                                    <th>Número</th>
                                    <th>Titular</th>
                                    <th>Parada</th>
                                    <th>Mes</th>
                                    <th class="text-right">Mercats</th>
                                    <th class="text-right">Tarifa</th>
                                    <th class="text-right">Total</th>
                                    <th class="text-center">Data cobrament</th>
                                    <th class="text-center">Pagat?</th>
                                    <th class="text-center">Reclamat?</th>
                                    <th class="text-center">Opcions</th>
                                </tr>
                            </thead>
                            @foreach ($invoices as $value)
                            <tr>
                                <td>{{$value->num}}</td>
                                <td><a href="{{ URL::route('owner.show', $value->ownerstall->owner->id) }}">{{$value->ownerstall->owner->dni}} - {{$value->ownerstall->owner->name}}</a></td>
                                <td><a href="{{ URL::route('stall.show', $value->ownerstall->stall->id) }}">{{$value->ownerstall->stall->num}}</a></td>
                                <td>{{$value->month}}</td>
                                <td class="text-right">{{$value->qty}}</td>
                                <td class="text-right">{{$value->price}} €</td>
                                <td class="text-right">{{$value->total}} €</td>
                                <th class="text-center">{{$value->paid}}</th>

                                @if (is_null($value->paid))
                                <td class="text-center warning">
                                    No
                                </td>
                                @else
                                <td class="text-center success" data-search="si">
                                    Si
                                </td>
                                @endif

                                {{ $value->claimed ? '<td class="text-center danger">Si</td>' : '<td class="text-center success">No</td>' }}

                                <td class="text-center" style='min-width:120px;'>
                                    {{ Form::open(array('url' => "invoice/{$value->id}")) }}
                                    <a class="btn btn-default btn-xs ttip confirm-print" href="{{ URL::route("invoice.printout", $value->id)}}" data-toggle="tooltip" data-placement="top" title="Imprimir rebut">
                                        <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                                    </a>
                                    <a class="btn btn-default btn-xs ttip" href="{{ URL::route("invoice.edit", $value->id)}}" data-toggle="tooltip" data-placement="top" title="Editar rebut">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                    <a class="btn btn-default btn-xs ttip" href="{{ URL::route("invoice.show", $value->id)}}" data-toggle="tooltip" data-placement="top" title="Veure fitxa">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn  btn-xs btn-danger confirm ttip', 'data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"Eliminar rebut", 'type'=>'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>

                </div>
            </div>

            
        </div>
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}


<script>
    $(document).ready(function () {
        $('.datatable').DataTable({
            language: spanish
        });
		
		// Javascript to enable link to tab
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
		} 
    });
</script>

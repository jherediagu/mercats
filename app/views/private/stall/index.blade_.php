@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}

<script type="text/javascript">
    
  $(function() {
    $( "#dialog" ).dialog();
  });

</script>

<div class="panel panel-default">
    <div class="panel-heading">
        Paradas
    </div>
    <div class="panel-body">

        @if ($stalls->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hay elementos
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Número</th>
                    <th>Longitud</th>
                    <th>Producto autorizado</th>
                    <th>Sector</th>
                </tr>
            </thead>
            @foreach ($stalls as $value)
            <tr>
                <td><b>#{{$value->num}}</b></td>
                <td>{{$value->length}} m</td>
                <td>{{$value->authprod->name}}</td>
                <td>{{$value->sector->name}}</td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

<div id="dialog" title="Basic dialog">
  <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}

<style>
    #datatable
    {
        margin-top: 20px !important;
        margin-bottom: 40px !important;
    }
</style>
<script>
    $('.confirm').click(function () {
        if (confirm("¿Desea eliminar el elemento?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    $(document).ready(function () {
        $('#datatable').DataTable({
            language: spanish,
            columnDefs: [{orderable: false, targets: [2]}]
        });
    });
</script>
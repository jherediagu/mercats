@include('private.header')

{{ HTML::style('assets/jquery-ui/jquery-ui.css'); }}

{{ HTML::style('assets/jquery-ui/bs-theme/jquery.ui.theme.css'); }}

{{ Form::model($ownerstall, array('route' => array('ownerstall.update', $ownerstall->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
<div class="panel panel-default center-block"  style="max-width:800px">
    <div class="panel-heading">Modificar dates alta i caducitat</div>
    <div class="panel-body">
        
        @include('private.error')
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Data d'alta</label>
                    <div class="col-sm-9">
                        {{ Form::input('date', 'discharge_date', date('Y-m-d', strtotime($ownerstall->discharge_date)), Array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Data de caducitat</label>
                    <div class="col-sm-9">
                        {{ Form::input('date', 'expire_date', date('Y-m-d', strtotime($ownerstall->expire_date)), Array('class' => 'form-control')) }}
                    </div>
                </div>
                
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Data de baixa</label>
                    <div class="col-sm-9">
                    @if(is_null($ownerstall->deactivated_at))

                        {{ Form::input('date', 'deactivated_at', '', Array('class' => 'form-control')) }}

                    @else
                        
                        {{ Form::input('date', 'deactivated_at', date('Y-m-d', strtotime($ownerstall->deactivated_at)), Array('class' => 'form-control')) }}
                    
                    @endif

                    </div>
                </div>
            </div>
        </div>
        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('ownerstall') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>
    </div>
</div>

{{ Form::close() }}


@include('private.footer')

{{ HTML::script('assets/jquery-ui/jquery-ui.js'); }}

<script>
    $(document).ready(function () {
        
    });
</script>

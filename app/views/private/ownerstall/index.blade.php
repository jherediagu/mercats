@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datepicker/dist/css/bootstrap-datepicker.css') }}

{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Històric d'assignacions
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('ownerstall.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Assignar parada
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        @if ($ownerstalls->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha històric
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Parada</th>
                    <th class="text-right">Metres</th>
                    <th>Producte</th>
                    <th>Sector</th>
                    <th>Tipus</th>
                    <th>Titular</th>
                    <th>Alta</th>
                    <th>Caducitat</th>
                    <th>Fi</th>
                    <th class="text-center">Desactivada</th>
                    <th class="text-center">Opcions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td id="col-0"></td>
                    <td id="col-1" class="text-center"></td>
                    <td id="col-2"></td>
                    <td id="col-3"></td>
                    <td id="col-4"></td>
                    <td id="col-5"></td>
                    <td id="col-6"></td>
                    <td id="col-7"></td>
                    <td id="col-8"></td>
                    <td id="col-9" class="text-center"></td>
                    <td id="col-10"></td>
                </tr>
            </tfoot>
            @foreach ($ownerstalls as $value)
            <tr> 
                <td><a href="{{ URL::route('stall.show', $value->stall->id) }}">{{$value->stall->num}}</a></td>
                <td class="text-right">{{$value->stall->length}}</td>
                <td>{{$value->stall->authprod->name}}</td>
                <td>{{$value->stall->sector->name}}</td>
                <td>{{$value->tariff->name}}</td>
                <td><a href="{{ URL::route('owner.show', $value->owner->id) }}">{{$value->owner->dni}} - {{$value->owner->name}}</a></td>
                <td>{{date('d/m/Y', strtotime($value->discharge_date))}}</td>
                <td>{{date('d/m/Y', strtotime($value->expire_date))}}</td>
                <td>{{is_null($value->deactivated_at) ? '-' : date('d/m/Y', strtotime($value->deactivated_at))}}</td>
                @if(is_null($value->deactivated_at))
                <td class="text-center success">No</td>
                @else
                <td class="text-center warning">Si</td>
                @endif
                
                <td class="text-center">
                    <a class='btn btn-default btn-xs ttip' href="{{ URL::route('ownerstall.edit', $value->id) }}" data-toggle="tooltip" data-placement="top" title="Editar històric">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                    {{ Form::open(array('url' => "ownerstall/{$value->id}")) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn  btn-xs btn-danger confirm', 'type'=>'submit']) }}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')


{{ HTML::script('assets/datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}
{{ HTML::script('assets/moment.min.js')}}

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<style>
    #datatable
    {
        margin-top: 20px !important;
        margin-bottom: 40px !important;
    }
    tfoot .form-control{
        width:100% !important;
    }
    
    a[href]::after {
        content: ""
    }
/*    tfoot{
        display: table-header-group;
    }*/
</style>
<script>
    $('.confirm').click(function () {
        if (confirm("Desitja donar de baixa el titular?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    function count(api, column, type)
    {
        var result = {active: 0, inactive: 0};

        api.column(column, type).data().each(function (v) {
            if (v == 'Si')
                result.active += 1;
            else if (v == 'No')
                result.inactive += 1;
        });
        return result;
    }
    
    $('#datatable tfoot th').each(function () {
        var title = $('#datatable thead th').eq($(this).index()).text();
        $(this).html('<input id="'+title+'" type="text" class="form-control" placeholder="' + title + '" />');
    });

    $(document).ready(function () {

                 $('#Alta').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });



         $('#Caducitat').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });



         $('#Fi').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });

        
        var table = $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            },
            columnDefs: [{orderable: false, targets: [10]}],
            footerCallback: function (row, data, start, end, display) {

                var api = this.api(), data;

                //var total = count(api, 7, {});
                //var current = count(api, 7, {page: 'current'});
                var filter = count(api, 9, {filter: 'applied'});

                $('#col-9').html(
//                        'Si (pagina): ' + current.active + '<br>' +
                        'Si: ' + filter.active + '<br>' +
//                        'Si (total): ' + total.active + '<hr>' +
//                        'No (pagina): ' + current.inactive + '<br>' +
                        'No: ' + filter.inactive + '<br>'
//                        + 'No (totals): ' + total.inactive + '<hr>'
                        );
                

            }
        });
        
        // Apply the search
        table.columns().eq(0).each(function (colIdx) {
            $('input', table.column(colIdx).footer()).on('keyup change', function () {
                table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
            });
        });
    });
</script>
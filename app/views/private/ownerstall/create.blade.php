@include('private.header')

{{ HTML::style('assets/jquery-ui/jquery-ui.css'); }}

{{ HTML::style('assets/jquery-ui/bs-theme/jquery.ui.theme.css'); }}

{{ Form::open(array('route' => "ownerstall.store", 'files' => true, 'class' => 'form-horizontal')) }}
<div class="panel panel-default center-block"  style="max-width:800px">
    <div class="panel-heading">Assignar titular a parada</div>
    <div class="panel-body">
        
        @include('private.error')
        
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Titular</label>
                    <div class="col-sm-9">
                        <div class="input-group ui-widget">
                            {{ Form::text('search1', null, Array('id' => 'inp-search-owner', 'class' => 'form-control', 'placeholder' => 'Buscar titular')); }}
                            <span class="input-group-addon" id="basic-addon2">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="div-owner" <?= (is_null($owner)) ? 'class="collapse"' : '' ?>>
                    {{ Form::hidden('owner_id', (!is_null($owner)) ? $owner->id : null, ['id' => 'hidden-owner']) }}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">D.N.I.</label>
                        <div class="col-sm-9">
                            <p id="dni" class="form-control-static">
                                @if (!is_null($owner))
                                <a href="{{ URL::to('owner', $owner->id) }}">
                                    {{$owner->dni}}
                                </a>
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nom</label>
                        <div class="col-sm-9">
                            <p id="name" class="form-control-static">{{ (!is_null($owner)) ? $owner->name : '' }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Direcció</label>
                        <div class="col-sm-9">
                            <p id="address" class="form-control-static">{{ (!is_null($owner)) ? $owner->address : '' }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">IBAN</label>
                        <div class="col-sm-9">
                            <p id="iban" class="form-control-static">{{ (!is_null($owner)) ? $owner->iban : '' }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Parada</label>
                    <div class="col-sm-9">
                        <div class="input-group ui-widget">
                            {{ Form::text('search2', null, Array('id' => 'inp-search-stall', 'class' => 'form-control', 'placeholder' => 'Buscar parada')); }}
                            <span class="input-group-addon" id="basic-addon2">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="div-stall" <?= (is_null($stall)) ? 'class="collapse"' : '' ?>>
                    {{ Form::hidden('stall_id', (!is_null($stall)) ? $stall->id : null, ['id' => 'hidden-stall']) }}

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número</label>
                        <div class="col-sm-9">
                            <p id="num" class="form-control-static">
                                @if (!is_null($stall))
                                <a href="{{ URL::to('stall', $stall->id) }}">
                                    {{$stall->num}}
                                </a>
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Longitut</label>
                        <div class="col-sm-9">
                            <p id="length" class="form-control-static">
                                {{ (!is_null($stall)) ? "{$stall->length}m" : '' }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sector</label>
                        <div class="col-sm-9">
                            <p id="sector" class="form-control-static">
                                {{ (!is_null($stall)) ? $stall->sector->name : '' }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Producte</label>
                        <div class="col-sm-9">
                            <p id="authprod" class="form-control-static">
                                {{ (!is_null($stall)) ? $stall->authprod->name : '' }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Assegurança</label>
                    <div class="col-sm-9">
                        {{ Form::file('insurance', Array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Data d'alta</label>
                    <div class="col-sm-9">
                        {{ Form::input('date', 'discharge_date', Input::old('discharge_date'), Array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Data de caducitat</label>
                    <div class="col-sm-9">
                        {{ Form::input('date', 'expire_date', Input::old('expire_date'), Array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tarifa</label>
                    <div class="col-sm-9">
                        {{ Form::select('tariff_id', $tariff, Input::old('tariff_id'), Array('class' => 'form-control')) }}
                    </div>
                </div>
                
            </div>
        </div>
        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('ownerstall') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>
    </div>
</div>

{{ Form::close() }}


@include('private.footer')

{{ HTML::script('assets/jquery-ui/jquery-ui.js'); }}

<script>
    $(document).ready(function () {
        $('#inp-search-owner').autocomplete({
            source: "{{ URL::to('owner/ajaxSearch') }}",
            minLength: 3,
            select: function (event, ui) {
                $('#hidden-owner').val(ui.item.id);
                
                $('#dni').html('<a href="<?= URL::to('owner') ?>/' + ui.item.id + '">' + ui.item.dni + '</a>');
                $('#name').html(ui.item.name);
                $('#address').html(ui.item.address);
                $('#iban').html(ui.item.iban);

                $('#div-owner').collapse('show');
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                    .append("<b>" + item.dni + "</b>: " + item.name)
                    .appendTo(ul);
        };

        $('#inp-search-stall').autocomplete({
            source: "{{ URL::to('stall/ajaxSearch') }}",
            minLength: 1,
            select: function (event, ui) {
                $('#hidden-stall').val(ui.item.id);
                
                $('#num').html('<a href="<?= URL::to('stall') ?>/' + ui.item.id + '">' + ui.item.num + '</a>');
                $('#length').html(ui.item.length + 'm');
                $('#sector').html(ui.item.sector.name);
                $('#authprod').html(ui.item.authprod.name);
                $('#div-stall').collapse('show');
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                    .append("Parada <b>" + item.num + "</b> de " + item.length + "m")
                    .appendTo(ul);
        };
    });
</script>

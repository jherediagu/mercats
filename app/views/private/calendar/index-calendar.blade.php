@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-4">
                Calendari
            </div>
            <div class='col-md-4 text-center'>
                <div class="btn-group" role="group">
                    <a href="{{ URL::route('calendar.index', ['view' => 'calendar']) }}" class="btn btn-default btn-xs active">Veure calendari</a>
                    <a href="{{ URL::route('calendar.index', ['view' => 'list']) }}" class="btn btn-default btn-xs">Veure llistat</a>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('calendar.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir dies
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        <div id="calendar"></div>

    </div>
</div>


<div class="modal fade">
  
</div>

@include('private.footer')

{{ HTML::script('assets/underscore-min.js'); }}
{{ HTML::script('assets/clndr/example/moment-2.8.3.js'); }}
{{ HTML::script('assets/clndr/src/clndr.js'); }}


{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}

<script type="text/template" id='calendar-template'>
    <div class="row">
    <div class="col-md-4">
    <button class="btn btn-default clndr-previous-button">&lsaquo;</button>
    </div>
    <div class="col-md-4 text-center"><%= month %> <%= year %></div>
    <div class="col-md-4 text-right">
    <button class="btn btn-default clndr-next-button">&rsaquo;</button>
    </div>
    </div>

    <div class="clndr-grid">
    <div class="row">
    <% _.each(daysOfTheWeek, function(day) { %>
    <div class="calendar-title"><%= day %></div>
    <% }); %>
    </div>

    <div class="days row">
    <% _.each(days, function(day) {  %>
    <div class="<%= day.classes %>">
        <%= day.day %>
    </div>
    <% }); %>
    </div>

    </div>
    </div>
</script>

<style>
    .today{
        color: #3c763d;
        background-color: #dff0d8;
        border-color: #d6e9c6;
    }
    .event
    {
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
        cursor:pointer;
    }
    .calendar-title
    {
        width: 14.28%;
        float: left;
        padding: 5px;
        border:1px solid #ddd;
        text-align:center;
        padding:20px;
        background-color: #f5f5f5;
    }

    .calendar-dow-0, .calendar-dow-1, .calendar-dow-2, .calendar-dow-3,
    .calendar-dow-4, .calendar-dow-5, .calendar-dow-6
    {
        width: 14.28%;
        float: left;
        border:1px solid #ddd;
        text-align:center;
        padding:20px;
    }

    .calendar-dow-0:hover, .calendar-dow-1:hover, .calendar-dow-2:hover, .calendar-dow-3:hover,
    .calendar-dow-4:hover, .calendar-dow-5:hover, .calendar-dow-6:hover
    {
        background-color: #f5f5f5;
    }

    .clndr-grid
    {
        margin:50px;
    }

</style>
<script>
    $(document).ready(function () {
        
        $('#calendar').clndr({
            showAdjacentMonths: true,
            adjacentDaysChangeMonth: false,
            startWithMonth: moment(),
            template: $('#calendar-template').html(),
            weekOffset: 1,
            daysOfTheWeek: ['Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte'],
            events: [
                @foreach ($days as $value)
                { date: "<?=date('Y-m-d', strtotime($value->date))?>", title: "Mercat", url: "<?=URL::route('calendar.show', $value->id)?>" },
                @endforeach
            ],
            clickEvents: {
                click: function (target) {
                    $.ajax({
                        type: "get",
                        url: target.events[0].url
                      })
                        .done(function( msg ) {
                            $('.modal').html(msg);
                            $('.modal').modal('show');
                    });
                }
            }
        });
    });
</script>

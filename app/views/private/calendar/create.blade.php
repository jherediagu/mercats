@include('private.header')


<div class="panel panel-default">
    <div class="panel-heading">
        Afegir dies al calendari
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::open(array('route' => "calendar.store", 'class' => 'form-horizontal')) }}
        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tot el mes</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            {{ Form::input('month', 'month', null, Array('class' => 'form-control', 'id' => 'input-month')); }}
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="btn-month">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>
                        <p class="help-block">S'afegiran tots els <b>{{$daysofweek[($cMarket->dayofweek - 1)]}}</b> del mes.</p>
                        <p class="help-block">Recordeu apretar el botó <b>+</b> per tal de fer efectiu l'entrada.</p>

                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Un dia</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            {{ Form::input('date', 'date', null, Array('class' => 'form-control', 'id' => 'input-date')); }}
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="btn-day">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>
                        <p class="help-block">Recordeu apretar el botó <b>+</b> per tal de fer efectiu l'entrada.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-5 div-dates">
                @foreach ((array)Input::old('days') as $value)
                    <div class="alert alert-info alert-dismissible text-center" role="alert"> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                    <p>Data per afegir: {{$value}}</p> 
                    <input type="hidden" name="days[]" value="{{$value}}"> 
                    </div>
                @endforeach
            </div>
        </div>

        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('calendar') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel·lar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')

<script>
    var dayofweek = <?= $cMarket->dayofweek ?>; 
    var days;

    function getDays(str, day) {
        var d = new Date(str);
        month = d.getMonth();
        days = [];
        d.setDate(1);
        
        while (d.getDay() !== day) {
            d.setDate(d.getDate() + 1);
        }

        while (d.getMonth() === month) {
            days.push(new Date(d.getTime()));
            d.setDate(d.getDate() + 7);
        }
        
        return days;
    }
    
    $('document').ready(function () {


        $('#btn-month').click(function () {
            getDays($('#input-month').val() + '-01', dayofweek).forEach(function (date) {

                var strdate = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
                if ($( "[value='" + strdate + "']" ).length === 0)
                {
                    var str = '<div class="alert alert-info alert-dismissible text-center" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<p>Fecha para añadir: ' + strdate + '</p>' +
                            '<input type="hidden" name="days[]" value="' + strdate + '">' +
                            '</div>';
                    $('.div-dates').append(str);
                }
            });
        });
        
        $('#btn-day').click(function () {
                var date = new Date($('#input-date').val());
                var strdate = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
                
                if ($( "[value='" + strdate + "']" ).length === 0 && !isNaN(date.getDate()))
                {
                    var str = '<div class="alert alert-info alert-dismissible text-center" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<p>Fecha para añadir: ' + strdate + '</p>' +
                            '<input id="strdate" type="hidden" name="days[]" value="' + strdate + '">' +
                            '</div>';
                    $('.div-dates').append(str);
                }
                else alert('La data no és vàlida');

        });

          $('form').on('submit', function(e){
            var day_u = '';
            day_u = $('#strdate').val()
            console.log(day_u);
            
            if(!days) {
                if(!day_u){
                    e.preventDefault();
                    alert ('No hi ha cap data afegida, al seleccionar data recordi apretar el botó "+"');
                }
            }
          });
    });
</script>


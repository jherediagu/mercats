<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Resum de parades actives el día {{$day->date}}</h4>
        </div>
        <div class="modal-body">
            <div role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#presence" aria-controls="presence" role="tab" data-toggle="tab">Absències</a></li>
                    <li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Incidències</a></li>
                    <li role="presentation"><a href="#absences" aria-controls="absence" role="tab" data-toggle="tab">Absències Justificades</a></li>
                </ul>

                <div class="tab-content">

                    <!-- PRESENCE -->
                    <div role="tabpanel" class="tab-pane active" id="presence">
                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Parada</th>
                                    <th>Titular</th>
                                    <th class="text-center">Asistencia</th>
                                </tr>
                            </thead>
                            @foreach ($presence as $value)
                            <tr>
                                <td><b>{{$value->stall->num}}</b></td>
                                <td>{{$value->owner->dni}}: {{$value->owner->name}}</td>
                                <td class="text-center"><span class="label label-success">Assisteix</span></td>
                            </tr>
                            @endforeach

                            @foreach ($missing as $value)
                            <tr>
                                <td><b>{{$value->stall->num}}</b></td>
                                <td>{{$value->owner->dni}}: {{$value->owner->name}}</td>
                                <td class="text-center"><span class="label label-warning">NO assisteix</span></td>
                            </tr>
                            @endforeach
                        </table>
                    </div>

                    <!-- NOTES -->
                    <div role="tabpanel" class="tab-pane" id="notes">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            @foreach ($general as $key => $value)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseGen{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                                                {{$value->title}}
                                            </a>
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <span class="label label-warning">General</span>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            @if (is_null($value->solved_at))
                                            <span class="label label-danger">No resolta</span>
                                            @else
                                            <span class="label label-success">Resolta</span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div id="collapseGen{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img src="" class="img-thumbnail img-responsive">
                                            </div>
                                            <div class="col-md-9 form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Creació</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">{{$value->created_at}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Data</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">{{$value->happens_at}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Titular</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">General</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Descripció</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static text-justify">{{nl2br($value->description)}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Enviat a</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">{{$value->sent_to}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Resolt</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static">
                                                            @if (is_null($value->solved_at))
                                                            No resolta
                                                            @else
                                                            Resolta el {{$value->solved_at}}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach

                            @foreach ($notes as $key => $value)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                                                {{$value->title}}
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            {{$value->ownerstall->stall->num}} - {{$value->ownerstall->owner->dni}} - {{$value->ownerstall->owner->name}}
                                        </div>
                                        <div class="col-md-2 text-center">
                                            @if (is_null($value->solved_at))
                                            <span class="label label-danger">No resolta</span>
                                            @else
                                            <span class="label label-success">Resolta</span>
                                            @endif
                                        </div>
                                        
                                    </div>
                                </div>
                                <div id="collapse{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="" class="img-thumbnail img-responsive">
                                        </div>
                                        <div class="col-md-9 form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Creació</label>
                                                <div class="col-sm-10">
                                                    <p class="form-control-static">{{$value->created_at}}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Data</label>
                                                <div class="col-sm-10">
                                                    <p class="form-control-static">{{$value->happens_at}}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Titular</label>
                                                <div class="col-sm-10">
                                                    <p class="form-control-static">{{$value->ownerstall->owner->dni}} - {{$value->ownerstall->owner->name}}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Descripció</label>
                                                <div class="col-sm-10">
                                                    <p class="form-control-static text-justify">{{nl2br($value->description)}}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Enviat a</label>
                                                <div class="col-sm-10">
                                                    <p class="form-control-static">{{$value->sent_to}}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Resolt</label>
                                                <div class="col-sm-10">
                                                    <p class="form-control-static">
                                                        @if (is_null($value->solved_at))
                                                            No resolta
                                                        @else
                                                            Resolta el {{$value->solved_at}}
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach



                        </div>
                    </div>
                    
                    <!-- ABSENCES -->
                    <div role="tabpanel" class="tab-pane" id="absences">
                        <table id="datatable2" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Parada</th>
                                    <th>Titular</th>
                                    <th>Absència</th>
                                </tr>
                            </thead>
                            @foreach ($absences as $value)
                            <tr>
                                <td><b>{{$value->ownerstall->stall->num}}</b></td>
                                <td>{{$value->ownerstall->owner->dni}}: {{$value->ownerstall->owner->name}}</td>
                                <td>Entre {{$value->start_at}} i {{$value->end_at}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    

    .tab-content { margin-top: 20px;}
</style>
<script>
    $(document).ready(function () {
        $('#datatable1').DataTable({
            language: spanish,
            columnDefs: [{orderable: false, targets: [2]}]
        });
        
        $('#datatable2').DataTable({
            language: spanish
        });
    });
</script>


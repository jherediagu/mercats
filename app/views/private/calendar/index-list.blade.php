@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-4">
                Calendari
            </div>
            <div class='col-md-4 text-center'>
                <div class="btn-group" role="group">
                    <a href="{{ URL::route('calendar.index', ['view' => 'calendar']) }}" class="btn btn-default btn-xs">Veure calendari</a>
                    <a href="{{ URL::route('calendar.index', ['view' => 'list']) }}" class="btn btn-default btn-xs active">Veure llistat</a>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('calendar.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir dies
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        @if ($days->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha dies
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th>Día</th>
                    <th class="text-center">Opcions</th>
                </tr>
            </thead>
            @foreach ($days as $value)
            <tr>
                <td data-order='{{strtotime($value->date)}}'>{{date('d-m-Y', strtotime($value->date))}}</td>
                <td class="text-center">
                    {{ Form::open(array('url' => "calendar/{$value->id}")) }}
                    <a class="btn btn-default btn-xs show-modal ttip" data-toggle="tooltip" data-placement="top" title="Veure resum" data-url="{{ URL::route("calendar.show", $value->id)}}">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-xs ttip" data-toggle="tooltip" data-placement="top" title="Afegir assistència" href="{{ URL::route("presence.create", ['calendar_id' => $value->id])}}">
                        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-xs ttip" data-toggle="tooltip" data-placement="top" title="Afegir incidència" href="{{ URL::route("notes.create", ['calendar_id' => $value->id])}}">
                        <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                    </a>
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn  btn-xs btn-danger confirm ttip', 'data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"Eliminar día", 'type'=>'submit']) }}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>



<div class="modal fade">

</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
    $('.show-modal').click(function () {
        $.ajax({
            type: "get",
            url: $(this).data('url')
        }).done(function (msg) {
            $('.modal').html(msg);
            $('.modal').modal('show');
        });
    });
    
    $('.confirm').click(function () {
        if (confirm("Desitja eliminar la data?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    $(document).ready(function () {

        $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
    });
</script>
@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datepicker/dist/css/bootstrap-datepicker.css') }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Absències
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('absence.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        @if ($absences->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha absències justificades
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Parada</th>
                    <th>Titular</th>
                    <th>Data inici</th>
                    <th>Data de fi</th>
                    <th>Document</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Parada</th>
                    <th>Titular</th>
                    <th>Data inici</th>
                    <th>Data de fi</th>
                    <th>Document</th>
                </tr>
            </tfoot>
            @foreach ($absences as $value)
            <tr>
                <td><a href="{{ URL::route('stall.show', $value->ownerstall->stall->id) }}">{{$value->ownerstall->stall->num}}</a></td>
                <td><a href="{{ URL::route('owner.show', $value->ownerstall->owner->id) }}">{{$value->ownerstall->owner->dni}} - {{$value->ownerstall->owner->name}}</a></td>
                <td>{{date('d-m-Y', strtotime($value->start_at))}}</td>
                <td>{{date('d-m-Y', strtotime($value->end_at))}}</td>
                <td>
                    @if($value->document != '')
                        <a href='{{url('docs/' . $value->document)}}' target='_blank'>Veure</a>
                    @else
                        -
                    @endif
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')


{{ HTML::script('assets/datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}
{{ HTML::script('assets/moment.min.js')}}


{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
        


        $('#datatable tfoot th').each(function () {
            var title = $('#datatable thead th').eq($(this).index()).text();
            if (title == 'Data inici') {
                $(this).html('<input id="datainici" type="text" class="form-control" placeholder="' + title + '" />');

            }else if (title == 'Data de fi'){
                $(this).html('<input id="datafinal" type="text" class="form-control" placeholder="' + title + '" />');

            }else{
                $(this).html('<input id="'+title+'" type="text" class="form-control" placeholder="' + title + '" />');
            }
        });
    $(document).ready(function () {




         $('#datainici').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });



         $('#datafinal').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });

        
        var table = $('#datatable').DataTable({
            language: spanish,
            dom: 'lTfrtip',
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
        
        // Apply the search
        table.columns().eq(0).each(function (colIdx) {
            $('input', table.column(colIdx).footer()).on('keyup change', function () {
                table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
            });
        });
    });
</script>
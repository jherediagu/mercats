@include('private.header')


{{ HTML::style('assets/jquery-ui/jquery-ui.css'); }}

{{ HTML::style('assets/jquery-ui/bs-theme/jquery.ui.theme.css'); }}

{{ HTML::style('assets/datepicker/dist/css/bootstrap-datepicker.css') }}

{{ HTML::style('assets/fileinput/css/fileinput.css'); }}

<div class="panel panel-default">
    <div class="panel-heading">
        Afegir absència justificada
    </div>
    <div class="panel-body">

        @if($errors->has())
        <div class="alert alert-danger text-center" role="alert">
            <p>
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                Ha de corregir els errors següents
            </p>
            @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
        </div>
        @endif

        {{ Form::open(array('route' => "absence.store", 'class' => 'form-horizontal', 'files' => true)) }}

        <div class="form-group">
            <label class="col-sm-3 control-label">Buscar</label>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group ui-widget">
                            {{ Form::text('search1', null, Array('id' => 'inp-search-owner', 'autofocus', 'class' => 'form-control', 'placeholder' => 'Buscar titular')); }}
                            <span class="input-group-addon" id="basic-addon2">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group ui-widget">
                            {{ Form::text('search2', null, Array('id' => 'inp-search-stall', 'class' => 'form-control', 'placeholder' => 'Buscar parada')); }}
                            <span class="input-group-addon" id="basic-addon2">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            </span>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        <div id="div-owner" <?= (is_null($ownerstall)) ? 'class="collapse"' : '' ?>>
            {{ Form::hidden('ownerstall_id', (!is_null($ownerstall)) ? $ownerstall->id : null, ['id' => 'hidden-ownerstall']) }}
            <div class="form-group">
                <label class="col-sm-3 control-label">D.N.I.</label>
                <div class="col-sm-9">
                    <p id="dni" class="form-control-static">
                        {{ (!is_null($ownerstall)) ? $ownerstall->owner->dni : '' }}
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nom</label>
                <div class="col-sm-9">
                    <p id="name" class="form-control-static">
                        {{ (!is_null($ownerstall)) ? $ownerstall->owner->name : '' }}
                    </p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Número</label>
                <div class="col-sm-9">
                    <p id="num" class="form-control-static">
                        {{ (!is_null($ownerstall)) ? $ownerstall->stall->num : '' }}
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Longitud</label>
                <div class="col-sm-9">
                    <p id="length" class="form-control-static">
                        {{ (!is_null($ownerstall)) ? "{$ownerstall->stall->length}m" : '' }}
                    </p>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Data d'alta</label>
                <div class="col-sm-9">
                    <p id="discharge_date" class="form-control-static">
                        {{ (!is_null($ownerstall)) ? $ownerstall->discharge_date : '' }}
                    </p>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Data de caducitat</label>
                <div class="col-sm-9">
                    <p id="expire_date" class="form-control-static">
                        {{ (!is_null($ownerstall)) ? $ownerstall->expire_date : '' }}
                    </p>
                </div>
            </div>
        </div>
        
        <div class="form-group" style="margin-top:15px;">
            <label class="col-sm-3 control-label">Data</label>
            <div class="col-sm-9">
                <div class="input-daterange input-group" id="datepicker">
                    {{ Form::text('start_at', Input::old('start_at'), Array('class' => 'form-control', 'required')); }}
                    <span class="input-group-addon">fins</span>
                    {{ Form::text('end_at', Input::old('end_at'), Array('class' => 'form-control', 'required')); }}
                </div>
                
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Document</label>
            <div class="col-sm-9">
                {{ Form::file('document', Array('id' => 'document', 'class' => 'form-control', 'multiple' => 'false')) }}
            </div>
        </div>
        
        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('absence') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/jquery-ui/jquery-ui.js'); }}
{{ HTML::script('assets/datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}

{{ HTML::script('assets/fileinput/js/fileinput.min.js'); }}

<script type="text/javascript">

    function fill(ui)
    {
        $('#hidden-ownerstall').val(ui.item.id);
        $('#dni').html(ui.item.owner.dni);
        $('#name').html(ui.item.owner.name);

        $('#num').html(ui.item.stall.num);
        $('#length').html(ui.item.stall.length + 'm');
        
        $('#discharge_date').html(ui.item.discharge_date);
        $('#expire_date').html(ui.item.expire_date);
        
        $('#div-owner').collapse('show');
    }
    $(document).ready(function () {


        $("#document").fileinput({
            //allowedFileTypes: ['text'],
            showUpload: false,
            maxFileCount: 1,
            overwriteInitial: true
        });
        
        $('#inp-search-owner').autocomplete({
            source: "{{ URL::to('ownerstall/ajaxOwnerSearch') }}",
            minLength: 2,
            select: function (event, ui) {
                fill(ui);
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                    .append("<b>" + item.stall.num + "</b>: " + item.owner.dni + " - " + item.owner.name)
                    .appendTo(ul);
        };
        

        $('#inp-search-stall').autocomplete({
            source: "{{ URL::to('ownerstall/ajaxStallSearch') }}",
            minLength: 2,
            select: function (event, ui) {
                fill(ui);
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                    .append("<b>" + item.stall.num + "</b>: " + item.owner.dni + " - " + item.owner.name)
                    .appendTo(ul);
        };

        $('.input-daterange').datepicker({
            format: 'dd-mm-yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });
    });
</script>



@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Titulars
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('owner.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir titular
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">

        @include('private.success')
        
        @if ($owners->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha titulars
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nº de parada</th>
                    <th>Nom</th>
                    <th>DNI</th>
                    <th>telefon</th>
                    <th>Correu</th>
                    <th class="text-center">Opcions</th>
                </tr>
            </thead>
            @foreach ($owners as $key => $value)
                  
                   @foreach($value->stalls as $stalls)
  
                         @if($stalls->market_id)

                            <tr>
                                <td>
                                    @foreach ($value->stalls as $stall)
                                        <a href="{{ URL::route('stall.show', $stall->id) }}">{{$stall->num}}</a>
                                    @endforeach
                                </td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->dni}}</td>
                                <td>{{$value->phone}}</td>
                                <td>{{$value->email}}</td>
                                <td class="text-center">
                                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Veure fitxa" href="{{ URL::route('owner.show', $value->id) }}">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Asignar parada" href="{{ URL::route('ownerstall.create', ['owner_id' => $value->id]) }}">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </a>
                                    <a class='btn btn-default btn-xs ttip' data-toggle="tooltip" data-placement="top" title="Editar titular" href="{{ URL::route('owner.edit', $value->id) }}">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>

                        @endif

                    @endforeach

            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
    $(document).ready(function () {
        $('#datatable').DataTable({
            dom: 'lTfrtip',
            language: spanish,
            columnDefs: [{orderable: false, targets: [3]}],
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            }
        });
    });
</script>
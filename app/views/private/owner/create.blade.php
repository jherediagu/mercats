@include('private.header')

<div class="panel panel-default">
    <div class="panel-heading">
        Afegir titular
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::open(array('route' => "owner.store", 'files' => true, 'class' => 'form-horizontal')) }}

        <h4>Dades del titular</h4>
        
        <div class="form-group">
            <label class="col-sm-3 control-label">Nom</label>
            <div class="col-sm-9">
                {{ Form::text('name', Input::old('name'), Array('class' => 'form-control', 'required', 'autofocus')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Correu electrònic</label>
            <div class="col-sm-9">
                {{ Form::email('email', Input::old('email'), Array('required', 'class' => 'form-control')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">D.N.I.</label>
            <div class="col-sm-9">
                {{ Form::text('dni', Input::old('dni'), Array('class' => 'form-control', 'required')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Direcció</label>
            <div class="col-sm-9">
                {{ Form::text('address', Input::old('address'), Array('class' => 'form-control', 'required')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Telèfon</label>
            <div class="col-sm-9">
                {{ Form::text('phone', Input::old('phone'), Array('class' => 'form-control', 'required')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">IBAN</label>
            <div class="col-sm-9">
                {{ Form::text('iban', Input::old('iban'), Array('class' => 'form-control', 'required')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Foto</label>
            <div class="col-sm-9">
                {{ Form::file('image', Array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Alta IAE</label>
            <div class="col-sm-9">
                {{ Form::file('pdf1', Array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Alta autònoms</label>
            <div class="col-sm-9">
                {{ Form::file('pdf2', Array('class' => 'form-control')) }}
            </div>
        </div>

        <hr>
        <h4>Dades del suplent</h4>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nom</label>
            <div class="col-sm-9">
                {{ Form::text('sname', Input::old('sname'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">D.N.I.</label>
            <div class="col-sm-9">
                {{ Form::text('sdni', Input::old('sdni'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Direcció</label>
            <div class="col-sm-9">
                {{ Form::text('saddress', Input::old('saddress'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Correu electrònic</label>
            <div class="col-sm-9">
                {{ Form::email('semail', Input::old('semail'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Telèfon</label>
            <div class="col-sm-9">
                {{ Form::text('sphone', Input::old('sphone'), Array('class' => 'form-control')); }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-3 control-label">Contracte</label>
            <div class="col-sm-9">
                {{ Form::file('pdf3', Array('class' => 'form-control')) }}
            </div>
        </div>
        
        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('owner') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel·lar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')



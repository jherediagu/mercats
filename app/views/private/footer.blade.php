
</div> 

<style>
    /* Styles go here */
    body {
        padding-top: 51px;
    }

    /* this is only to style the sidebar - you can make the sidebar look any way you want */
    .sidebar {
        padding: 20px;
        background-color: #0A5F90;
        /*border-right: 2px solid #7196AB; */
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
      
    }

    .sidebar > ul > li > a{
        color: #6B9EBC;
    }

        .sidebar > ul > li > a:hover{
        background-color: #084c73;
    }


            .open:focus{
        background-color: #084c73;
    }
    /* All the default sidebar styles */
    /* toggle button settings */
    /* Note: this disables navbar-toggle class hide function. set visibility with the boostrap visible/hidden classes */
    .navbar.navbar-static .navbar-header {
        float: left;
    }

    .navbar .navbar-toggle.toggle-left {
        float: left;
        margin-left: 15px;
    }

    .navbar .navbar-toggle.toggle-right {
        float: right;
        margin-right: 15px;
    }

    .navbar .navbar-toggle.toggle-sidebar, [data-toggle="sidebar"] {
        display: block;
    }

    /* sidebar settings */
    .sidebar {
        position: fixed;
        display: block;
        top: 51px;
        bottom:0;
        z-index: 1000;
        min-height: 100%;
        max-height: none;
        overflow: auto;
    }

    .sidebar-left {
        left: 0;
    }

    .sidebar-right {
        right: 0;
    }

    /* css to override hiding the sidebar according to different screen sizes */  
    .row .sidebar.sidebar-left.sidebar-xs-show {
        left: 0;
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
        -moz-transform: none;
    }

    /*right sidebar is untested */
    .row .sidebar.sidebar-right.sidebar-xs-show {
        right: 0;
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
        -moz-transform: none;
    }  

    @media (min-width: 768px) {
        .row .sidebar.sidebar-left.sidebar-sm-show {
            left: 0;
            -webkit-transform: translate3d(0,0,0);
            transform: translate3d(0,0,0);
            -moz-transform: none;
        }

        /*right sidebar is untested */
        .row .sidebar.sidebar-right.sidebar-sm-show {
            right: 0;
            -webkit-transform: translate3d(0,0,0);
            transform: translate3d(0,0,0);
            -moz-transform: none;
        }  
    } 

    @media (min-width: 992px) {
        .row .sidebar.sidebar-left.sidebar-md-show {
            left: 0;
            -webkit-transform: translate3d(0,0,0);
            transform: translate3d(0,0,0);
            -moz-transform: none;
        }
        .row .sidebar.sidebar-right.sidebar-md-show {
            right: 0;
            -webkit-transform: translate3d(0,0,0);
            transform: translate3d(0,0,0);
            -moz-transform: none;
        }
    }

    @media (min-width: 1170px) {
        .row .sidebar.sidebar-left.sidebar-lg-show {
            left: 0;
            -webkit-transform: translate3d(0,0,0);
            transform: translate3d(0,0,0);
            -moz-transform: none;
        }
        .row .sidebar.sidebar-right.sidebar-lg-show {
            right: 0;
            -webkit-transform: translate3d(0,0,0);
            transform: translate3d(0,0,0);
            -moz-transform: none;
        }
    }

    /* animation class - optional: without it the sidebar would just pop in and out*/
    .sidebar-animate {
        -webkit-transition: -webkit-transform 300ms ease;
        -moz-transition: -moz-transform 300ms ease;
        transition: transform 300ms ease;
    }

    /* Left panel positioning classes */
    .sidebar.sidebar-left {
        -webkit-transform: translate3d(-100%,0,0);
        -moz-transform: translate3d(-100%,0,0);
        transform: translate3d(-100%,0,0);
    }

    .sidebar.sidebar-left.sidebar-open {
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
        -moz-transform: none;
    }

    /* TODO: TEST THIS! Right panel positioning classes*/
    .sidebar.sidebar-right {
        -webkit-transform: translate3d(100%,0,0);
        -moz-transform: translate3d(100%,0,0);
        transform: translate3d(100%,0,0);
    }

    .sidebar.sidebar-right.sidebar-open {
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
        -moz-transform: none;
    }
</style>

{{ HTML::script('assets/jquery/jquery-2.1.3.min.js'); }}
{{ HTML::script('assets/bs/js/bootstrap.min.js'); }}

<script>
    $(document).ready(function () {
        $('.ttip').tooltip();

        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    });

</script>
</body>
</html>

<script>
    /* ========================================================================
     * Bootstrap: sidebar.js v0.1
     * ========================================================================
     * Copyright 2011-2014 Asyraf Abdul Rahman
     * Licensed under MIT
     * ======================================================================== */

    +function ($) {
        'use strict';

        // SIDEBAR PUBLIC CLASS DEFINITION
        // ================================

        var Sidebar = function (element, options) {
            this.$element = $(element)
            this.options = $.extend({}, Sidebar.DEFAULTS, options)
            this.transitioning = null

            if (this.options.parent)
                this.$parent = $(this.options.parent)
            if (this.options.toggle)
                this.toggle()
        }

        Sidebar.DEFAULTS = {
            toggle: true
        }

        Sidebar.prototype.show = function () {
            if (this.transitioning || this.$element.hasClass('sidebar-open'))
                return


            var startEvent = $.Event('show.bs.sidebar')
            this.$element.trigger(startEvent);
            if (startEvent.isDefaultPrevented())
                return

            this.$element
                    .addClass('sidebar-open')

            this.transitioning = 1

            var complete = function () {
                this.$element
                this.transitioning = 0
                this.$element.trigger('shown.bs.sidebar')
            }

            if (!$.support.transition)
                return complete.call(this)

            this.$element
                    .one($.support.transition.end, $.proxy(complete, this))
                    .emulateTransitionEnd(400)
        }

        Sidebar.prototype.hide = function () {
            if (this.transitioning || !this.$element.hasClass('sidebar-open'))
                return

            var startEvent = $.Event('hide.bs.sidebar')
            this.$element.trigger(startEvent)
            if (startEvent.isDefaultPrevented())
                return

            this.$element
                    .removeClass('sidebar-open')

            this.transitioning = 1

            var complete = function () {
                this.transitioning = 0
                this.$element
                        .trigger('hidden.bs.sidebar')
            }

            if (!$.support.transition)
                return complete.call(this)

            this.$element
                    .one($.support.transition.end, $.proxy(complete, this))
                    .emulateTransitionEnd(400)
        }

        Sidebar.prototype.toggle = function () {
            this[this.$element.hasClass('sidebar-open') ? 'hide' : 'show']()
        }

        var old = $.fn.sidebar

        $.fn.sidebar = function (option) {
            return this.each(function () {
                var $this = $(this)
                var data = $this.data('bs.sidebar')
                var options = $.extend({}, Sidebar.DEFAULTS, $this.data(), typeof options == 'object' && option)

                if (!data && options.toggle && option == 'show')
                    option = !option
                if (!data)
                    $this.data('bs.sidebar', (data = new Sidebar(this, options)))
                if (typeof option == 'string')
                    data[option]()
            })
        }

        $.fn.sidebar.Constructor = Sidebar

        $.fn.collapse.noConflict = function () {
            $.fn.sidebar = old
            return this
        }

        $(document).on('click.bs.sidebar.data-api', '[data-toggle="sidebar"]', function (e) {
            var $this = $(this), href
            var target = $this.attr('data-target')
                    || e.preventDefault()
                    || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')
            var $target = $(target)
            var data = $target.data('bs.sidebar')
            var option = data ? 'toggle' : $this.data()

            $target.sidebar(option)
        })

        $('html').on('click.bs.sidebar.autohide', function (event) {
            var $this = $(event.target);
            var isButtonOrSidebar = $this.is('.sidebar, [data-toggle="sidebar"]') || $this.parents('.sidebar, [data-toggle="sidebar"]').length;
            if (isButtonOrSidebar) {
                return;
            } else {
                var $target = $('.sidebar');
                $target.each(function (i, trgt) {
                    var $trgt = $(trgt);
                    if ($trgt.data('bs.sidebar') && $trgt.hasClass('sidebar-open')) {
                        $trgt.sidebar('hide');
                    }
                })
            }
        });
    }(jQuery);

</script>
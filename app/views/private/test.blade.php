@include('private.header')

<div id="webcam"></div>

<a href="javascript:webcam.capture();changeFilter();void(0);">Take a picture</a>

<canvas id="canvas" height="240" width="320"></canvas>

@include('private.footer')

{{ HTML::script('assets/webcam/jquery.webcam.js'); }}

<script>
jQuery("#webcam").webcam({

    width: 320,
    height: 240,
    mode: "callback",
    swffile: "{{url('mercats/public/assets/webcam/jscam_canvas_only.swf')}}/", // canvas only doesn't implement a jpeg encoder, so the file is much smaller

    onTick: function(remain) {

        if (0 == remain) {
            jQuery("#status").text("Cheese!");
        } else {
            jQuery("#status").text(remain + " seconds remaining...");
        }
    },

    onSave: function(data) {

        var col = data.split(";");
    // Work with the picture. Picture-data is encoded as an array of arrays... Not really nice, though =/
    },

    onCapture: function () {
        webcam.save();

      // Show a flash for example
    },

    debug: function (type, string) {
        // Write debug information to console.log() or a div, ...
    },

    onLoad: function () {
    // Page load
        var cams = webcam.getCameraList();
        for(var i in cams) {
            jQuery("#cams").append("<li>" + cams[i] + "</li>");
        }
    }
});

</script>
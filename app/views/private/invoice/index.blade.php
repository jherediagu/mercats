@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datepicker/dist/css/bootstrap-datepicker.css') }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Rebuts
            </div>

            <div class="col-md-4 text-right">
                <a href="{{ URL::route('invoice.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Generar rebuts
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

            <div class="col-md-12 text-right">
                <a class="btn btn-default btn-md ttip confirm-print" href="{{ URL::route('invoice.notpaid') }}">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Veure rebuts no pagats
                </a>
            </div>
        @if ($invoices->isEmpty())
        <div class="alert alert-info text-center" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            No hi ha rebuts
        </div>
        @else
        <table id="datatable" class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th>Número</th>
                    <th>Titular</th>
                    <th>Parada</th>
                    <th>Mes</th>
                    <th class="text-right">Dia mercat</th>
                    <th class="text-right">Tarifa</th>
                    <th class="text-right">Total</th>
                    <th class="text-center">Pagat?</th>
                    <th class="text-center">Data cobrament</th>
                    <th class="text-center">Reclamat?</th>
                    <th class="text-center">Opcions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Número</th>
                    <th>Titular</th>
                    <th>Parada</th>
                    <th>Mes</th>
                    <th>Dia mercat</th>
                    <th>Tarifa</th>
                    <th>Total</th>
                    <th>Pagat?</th>
                    <th>Data cobrament</th>
                    <th>Reclamat?</th>
                    <th></th>
                </tr>
                <tr>
                    <td id="col-0"></td>
                    <td id="col-1"></td>
                    <td id="col-2"></td>
                    <td id="col-3" ></td>
                    <td id="col-4"></td>
                    <td id="col-5"></td>
                    <td id="col-6" class="text-right"></td>
                    <td id="col-7"></td>
                    <td id="col-8"></td>
                    <td id="col-9"></td>
                    <td id="col-10"></td>
                </tr>
            </tfoot>
            @foreach ($invoices as $value)
            <tr>
                <td>{{$value->num}}</td>
                <td><a href="{{ URL::route('owner.show', $value->ownerstall->owner->id) }}">{{$value->ownerstall->owner->dni}} - {{$value->ownerstall->owner->name}}</a></td>
                <td><a href="{{ URL::route('stall.show', $value->ownerstall->stall->id) }}">{{$value->ownerstall->stall->num}}</a></td>
                <td>{{$value->month}}</td>
                <td class="text-right">{{$value->ownerstall->stall->market->name}}</td>
                <td class="text-right">{{$value->price}} €</td>
                <td class="text-right">{{$value->total}} €</td>

                @if (is_null($value->paid))
                <td class="text-center warning" data-search="no">
                    No
                </td>
                @else
                <td class="text-center success" data-search="si">
                    Si
                </td>
                @endif

                <td class="text-center">{{$value->paid}}</td> 
                
                {{ $value->claimed ? '<td class="text-center danger">Si</td>' : '<td class="text-center success">No</td>' }}
                
                <td class="text-center" style='min-width:120px;'>
                    {{ Form::open(array('url' => "invoice/{$value->id}")) }}
                    <!--<a class="btn btn-default btn-xs ttip confirm-print" href="{{ URL::route("invoice.printout", $value->id)}}" data-toggle="tooltip" data-placement="top" title="Imprimir rebut">
                        <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                    </a>-->
                    <a class="btn btn-default btn-md ttip confirm-print" href="{{ URL::route('invoice.printandpay', $value->id) }}" data-toggle="tooltip" data-placement="top" title="Imprimir i marcar cobrat">
                        <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-md ttip confirm-print" href="mercats.scheme://mercagest.com/gestio/public/print/{{$value->id}}/{{$token}}" data-toggle="tooltip" data-placement="top" title="Imprimir rebut">
                        <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-md ttip" href="{{ URL::route("invoice.edit", $value->id)}}" data-toggle="tooltip" data-placement="top" title="Comentar reclamació">
                        <span class="glyphicon glyphicon-flash" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-md ttip" href="{{ URL::route("invoice.show", $value->id)}}" data-toggle="tooltip" data-placement="top" title="Veure fitxa">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    </a>
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['class'=>'btn  btn-md btn-danger confirm ttip', 'data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"Eliminar rebut", 'type'=>'submit']) }}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}
{{ HTML::script('assets/moment.min.js')}}


{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}

<script>
    /*
    $('.confirm-print').click(function () {
        if (confirm("Desitja imprimir i marcar com a pagat?") === false)
        {
            event.preventDefault();
            return false;
        }
    });
*/
    $('.confirm').click(function () {
        if (confirm("Desitja eliminar el rebut?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    function sum(api, column, type)
    {
        var result = 0.00;

        var column = api.column(column, type).data();
        
        column.each(function(v, k){
            result += parseFloat(v);
        });
        return result;
    }

    $(document).ready(function () {

        $('#datatable tfoot th').each(function () {
            var title = $('#datatable thead th').eq($(this).index()).text();
            $(this).html('<input id="'+title+'"type="text" class="form-control" placeholder="' + title + '" />');
        });


         $('#Data').datepicker({
            format: 'dd/mm/yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });

        // DataTable
        var table = $('#datatable').DataTable({
            language: spanish,
            dom: 'lTfrtip',
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            },
            footerCallback: function (row, data, start, end, display) {

                var api = this.api(), data;
                
                //var total = sum(api, 1, 7, {});
                //var current = sum(api, 1, 7, {page: 'current'});
                var filter = sum(api, 6, {filter: 'applied'});


                var total = (filter).toFixed(2);
                
                $('#col-6').html(
                        '<h4><span class="label label-success">Total: ' + total + '€</span></h4>'
                );

            }
        });

    
        // Apply the search
        table.columns().eq(0).each(function (colIdx) {
            $('input', table.column(colIdx).footer()).on('keyup change', function () {
                table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
            });
        });
    });
</script>
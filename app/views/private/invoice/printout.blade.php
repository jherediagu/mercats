<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <title>Administración</title>
        {{ HTML::style('assets/bs/css/bootstrap.min.css'); }}
        {{ HTML::style('assets/css/styles.css'); }}
        {{ HTML::script('assets/jquery/jquery-2.1.3.min.js'); }}
    </head>

    <style>
        body {
            margin:50px;
            max-width:800px;
        }

    </style>

    <script type="text/javascript">
        function pagarImprimir(){
            $.ajax({
                type: "GET",
                url: '{{ URL::route("invoice.payprint", $invoice->id)}}',
                dataType: "json"
            })
            .done(function(data) {
                if(data.res == "ok"){
                    window.print();
                }else{
                    alert("Error marcant el rebut com a pagat");
                }
            })
            .fail(function() {
                alert("Error marcant el rebut com a pagat");
            })
        }
    </script>

    <body>
        <a href="javascript:pagarImprimir();">Pagar i imprimir</a> &middot;<a href="{{ URL::route("invoice.index")}}">Tornar</a>
        <table class="table">
            <tr>
                <td>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Rebut</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{$invoice->num}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Data</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{$invoice->paid}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Parada</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">{{$invoice->ownerstall->stall->num}}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Titular</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                {{$invoice->ownerstall->owner->dni}}<br>
                                {{$invoice->ownerstall->owner->name}}<br>
                                {{$invoice->ownerstall->owner->address}}
                            </p>
                        </div>
                    </div>
                </td>
                <td>
                    {{$cMarket->name}} <br>
                    {{$cMarket->town->name}}
                </td>
            </tr>

        </table>
        <table class="table">
            <thead>
                <tr>
                    <td>Mes</td>
                    <td>Numero de mercats</td>
                    <td>Metres de la prada</td>
                    <td>Preu per metre</td>
                    <td>Total</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$invoice->month}}</td>
                    <td>{{$invoice->qty}}</td>
                    <td>{{$invoice->ownerstall->stall->length}}m</td>
                    <td>{{$invoice->price}}€</td>
                    <td>{{$invoice->total}}€</td>
                </tr>
            </tbody>
        </table>

    </body>
</html>

@include('private.header')

<div class="panel panel-default">
    <div class="panel-heading">
        Generar rebuts
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::open(array('route' => "invoice.store", 'class' => 'form-horizontal')) }}
        <div class="form-group">
            <label class="col-sm-3 control-label">Mes</label>
            <div class="col-sm-9">
                    {{ Form::input('month', 'month', null, Array('class' => 'form-control', 'id' => 'input-month')); }}
            </div>
        </div>

        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('invoice') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Generar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')

<script>
    var dayofweek = <?= $cMarket->dayofweek ?>;

    function getDays(str, day) {
        var d = new Date(str);
        month = d.getMonth();
        days = [];
        d.setDate(1);

        while (d.getDay() !== day) {
            d.setDate(d.getDate() + 1);
        }

        while (d.getMonth() === month) {
            days.push(new Date(d.getTime()));
            d.setDate(d.getDate() + 7);
        }

        return days;
    }

    $('document').ready(function () {
        $('#btn-month').click(function () {
            getDays($('#input-month').val() + '-01', dayofweek).forEach(function (date) {

                var str = '<div class="alert alert-info alert-dismissible text-center" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        '<p>Fecha para añadir: ' + date.toLocaleDateString() + '</p>' +
                        '<input type="hidden" name="days[]" value="' + date.toLocaleDateString() + '">' +
                        '</div>'
                $('.div-dates').append(str);
            });
        });
    });
</script>


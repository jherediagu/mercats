@include('private.header')

<div class="panel panel-default">
    <div class="panel-heading">
        <div class='row'>
            <div class='col-md-6'>
                Fitxa de rebut
            </div>
            <div class='col-md-6 text-right'>
                <a href="javascript:window.print();">Imprimir</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class='row'>
            <div class='col-md-6 form-horizontal'>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Rebut</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{$invoice->num}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Data</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{$invoice->paid}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Parada</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{$invoice->ownerstall->stall->num}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Titular</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            {{$invoice->ownerstall->owner->dni}} -
                            {{$invoice->ownerstall->owner->name}} -
                            {{$invoice->ownerstall->owner->address}}
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pagat</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{is_null($invoice->paid) ? 'No' : 'Si: ' . $invoice->paid}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Reclamat</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{ ($invoice->claimed == 0) ? 'No' : 'Si' }}</p>
                    </div>
                </div>
            </div>
            <div class='col-md-6'>
                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="form-control-static"><h4>{{$cMarket->name}}</h4></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="form-control-static">{{$cMarket->town->name}}</p>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered" style="margin-top:50px;">
            <thead>
                <tr>
                    <td>Mes</td>
                    <td>Numero de mercats</td>
                    <td>Metres de la prada</td>
                    <td>Preu per metre</td>
                    <td>Total</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$invoice->month}}</td>
                    <td>{{$invoice->qty}}</td>
                    <td>{{$invoice->ownerstall->stall->length}}m</td>
                    <td>{{$invoice->price}}€</td>
                    <td>{{$invoice->total}}€</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@include('private.footer')

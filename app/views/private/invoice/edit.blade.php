@include('private.header')

<div class="panel panel-default">
    <div class="panel-heading">
        Reclamar rebut
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::model($invoice, array('route' => array('invoice.update', $invoice->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            <label class="col-sm-3 control-label">Reclamat</label>
            <div class="col-sm-9">
                    {{ Form::textarea('claimed', Input::old('claimed'), Array('class' => 'form-control')); }}
            </div>
        </div>

        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('invoice') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancelar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')

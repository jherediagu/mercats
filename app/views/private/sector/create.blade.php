@include('private.header')

<div class="panel panel-default">
    <div class="panel-heading">
        Afegir sector
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::open(array('route' => "sector.store", 'class' => 'form-horizontal')) }}

        <div class="form-group">
            <label class="col-sm-3 control-label">Nom</label>
            <div class="col-sm-9">
                {{ Form::text('name', Input::old('name'), Array('class' => 'form-control', 'required', 'autofocus')); }}
            </div>
        </div>

        <div class="text-right" style="margin-top:50px;">
            <a href="{{ URL::to('sector') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel·lar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>

@include('private.footer')



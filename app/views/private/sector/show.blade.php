@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}

<div class="panel panel-default">
    <div class="panel-heading">
        Editar sector
    </div>
    <div class="panel-body">

        @include('private.error')

        {{ Form::model($sector, array('route' => array('sector.update', $sector->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            <label class="col-sm-2 control-label">Nom</label>
            <div class="col-sm-6">
                {{ Form::text('name', Input::old('name'), Array('class' => 'form-control', 'required', 'autofocus')); }}
            </div>
            <div class="col-sm-4">

            <a href="{{ URL::to('sector') }}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel·lar</a>
            {{ Form::button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar', ['class'=>'btn btn-default', 'type'=>'submit']) }}
            </div>
        </div>



        {{ Form::close() }}

    <div class="form-group">
        <br>
        <label class="control-label">Agefir productes al sector</label>
        <br><small>per veure els afegits escriure en el filtre: "afegits"</small>
        <br><br>
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th class="text-center">Afegir</th>
                </tr>
            </thead>
            @foreach ($authprod as $key => $value)
            <tr>
                <td>{{$value->name}}</td>
                <td class="text-center">
                    {{ Form::open(array('url' => "authprod/{$value->id}")) }} 

                    @if ( in_array($value->id, $rel_prod_sectors))
                    <button class='btn btn-default btn-sm delete_product' value="{{ $value->id }}">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"><span style="display:none">afegits</span></span>
                    </button>
                    @else
                    <button class='btn btn-default btn-sm add_product'  value="{{ $value->id }}">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"><span style="display:none">afegir</span></span>
                    </button>
                    @endif
                </td>
            </tr>
            @endforeach
        </table>

    </div>

    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}




<script>
    $('.confirm').click(function () {
        if (confirm("Desitja eliminar el producte autoritzat?") === false)
        {
            event.preventDefault();
            return false;
        }
    });

    $(document).ready(function () {
        $('#datatable').DataTable({
            language: spanish,
            paging:   true,
            info:     false,
            "order": [ [ 1, 'desc' ]]
     
        });
        

        $('.add_product').click(function(){                  
            var currentValue = $(this).attr("value");
            console.log(currentValue);
            $.ajax({
                url: 'editProducts/{{ $sector->id }}',
                method: 'post',             
                data: {id_product: currentValue },
                success: function(data){
                    location.reload();
                },
                error: function(data){
                    alert(currentValue);
                },
            });
        }); 

        $('.delete_product').click(function(){                  
            var currentValue = $(this).attr("value");
            console.log(currentValue);
            $.ajax({
                url: 'deleteProducts/{{ $sector->id }}',
                method: 'post',             
                data: {id_product: currentValue },
                success: function(data){
                    location.reload();
                },
                error: function(data){
                    alert(currentValue);
                },
            });
        });   
    });

</script>

@include('private.header')

{{ HTML::style('assets/datatables/media/css/dataTables.bootstrap.css'); }}
{{ HTML::style('assets/datepicker/dist/css/bootstrap-datepicker.css') }}
{{ HTML::style('assets/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); }}
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                Assistència
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ URL::route('presence.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Afegir
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        @include('private.success')

        <style>
            .display-inline{display:inline;}
        </style>
        <table id="datatable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Parada</th>
                    <th class="text-center">Titular</th>
                    <th class="text-center">Activa</th>
                    <th class="text-center">Assisteix</th>
                </tr>
            </thead>
            <tfoot>
                <tr id='tfoot-filter'>
                    <th>Data</th>
                    <th>Parada</th>
                    <th class="text-center">Titular</th>
                    <th class="text-center">Activa</th>
                    <th class="text-center">Assisteix</th>
                </tr>
                <tr>
                    <td id="col-0">
                        <div class="input-daterange" id="datepicker">
                            <input type="text" class="form-control date-range" id="min" placeholder="Desde">
                            <br>
                            <input type="text" class="form-control date-range" id="max" placeholder="Fins">
                            <button id="filter" class="btn btn-xs btn-default">Filtrar</button>
                        </div>

                    </td>
                    <td id="col-1"></td>
                    <td id="col-2"></td>
                    <td id="col-3" class="text-center"></td>
                    <td id="col-4" class="text-center"></td>
                </tr>
            </tfoot>

            @foreach ($calendar as $day)
                @foreach ($stall as $st)

                    <?php $key = "{$day->id}-{$st->id}"; ?>

                    @if (isset($np2[$key]))

                        <tr>
                            <td data-order="{{strtotime($day->date)}}">{{date('d/m/Y', strtotime($day->date))}}</td>
                            <td>
                                <a href="{{URL::route('stall.show', $st->id)}}">
                                    {{ $st->num }}
                                </a>
                            </td>
                            @if (isset($np["{$day->id}-{$st->id}"]))
                            <td class="text-center">
                                <a href="{{URL::route('owner.show', $np[$key]->owner->id)}}">
                                    {{ $np[$key]->owner->dni }} - {{$np[$key]->owner->name}}
                                </a>
                            </td>

                            <td class="text-center success">Si</td>
                            @if (isset($np2[$key]))
                            <td class="text-center danger">No</td>
                            @else
                            <td class="text-center success">
                                Si
                                
                                {{ Form::open(array('route' => "presence.store", 'class' => 'form-horizontal display-inline')) }}
                                {{ Form::hidden('ownerstall_id', $np[$key]->id) }}
                                {{ Form::hidden('date', $day->date); }}
                                {{ Form::button('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', ['class'=>'btn btn-default pull-right btn-xs ttip', 'title' => 'Afegir no assistència', 'type'=>'submit']) }}

                            </td>
                            @endif
                            @else
                            <td class="text-center">
                                -
                            </td>
                            <td class="text-center warning">No</td>
                            <td class="text-center">
                                -
                            </td>
                            @endif
                        </tr>
                    
                    @endif

                @endforeach
            @endforeach
        </table>

    </div>
</div>

@include('private.footer')

{{ HTML::script('assets/datatables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('assets/datatables/media/js/dataTables.bootstrap.js'); }}
{{ HTML::script('assets/datatables/media/js/Spanish.js'); }}

{{ HTML::script('assets/datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}
{{ HTML::script('assets/moment.min.js')}}
{{ HTML::script('assets/datatables/extensions/dataTables.tableTools.min.js')}}


<script>

    function count(api, column, type)
    {
        var result = {active: 0, inactive: 0};

        api.column(column, type).data().each(function (v) {
            if (v == 'Si')
                result.active += 1;
            else if (v == 'No')
                result.inactive += 1;
        });
        return result;
    }


    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = moment($('#min').val(), "DD-MM-YYYY").isValid() ?  moment($('#min').val(), "DD-MM-YYYY") : moment("1970-01-01");
                var max = moment($('#max').val(), "DD-MM-YYYY").isValid() ?  moment($('#max').val(), "DD-MM-YYYY") : moment("2300-01-01");
                var date = moment(data[0], "DD-MM-YYYY");
                
                return (date.isBetween(min, max));
            }
    );

    $(document).ready(function () {

        $('.input-daterange').datepicker({
            format: 'dd-mm-yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });


                $('#Data').datepicker({
            format: 'dd-mm-yyyy',
            language: "es",
            weekStart: 1,
            orientation: 'top auto',
            changeMonth: true,
            changeYear: false
        });


        $('#datatable tfoot th').each(function () {
            var title = $('#datatable thead th').eq($(this).index()).text();
            $(this).html('<input id="'+title+' "type="text" class="form-control" placeholder="' + title + '" />');
        });

        $('#filter').click(function () {
            table.draw();
        });

        // DataTable
        var table = $('#datatable').DataTable({
            dom: "lTfrtip",
            language: spanish,
            tableTools: {
                    sSwfPath: "{{ URL::asset('assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') }}",
                    aButtons: [
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'print',
                            sButtonText: 'Imprimir',
                            sInfo: "<h4>Utilitzi el menú de l'explorador per imprimir la página</h4>Premi [Esc] per sortir"
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'xls',
                            sFileName: "*.xls",
                            sButtonText: 'XLS'
                        },
                        {
                            oSelectorOpts: { filter: 'applied', order: 'current' },
                            sExtends: 'csv',
                            sButtonText: 'CSV'
                        }
                    ]
            },
            footerCallback: function (row, data, start, end, display) {

                var api = this.api(), data;

                var total = count(api, 3, {});
                var current = count(api, 3, {page: 'current'});
                var filter = count(api, 3, {filter: 'applied'});

                $('#col-3').html(
                        'Si (pagina): ' + current.active + '<br>' +
                        'Si (resultats): ' + filter.active + '<br>' +
                        'Si (total): ' + total.active + '<hr>' +
                        'No (pagina): ' + current.inactive + '<br>' +
                        'No (resultats): ' + filter.inactive + '<br>' +
                        'No (totals): ' + total.inactive + '<hr>'
                        );

                var total = count(api, 4, {});
                var current = count(api, 4, {page: 'current'});
                var filter = count(api, 4, {filter: 'applied'});


                $('#col-4').html(
                        'Si (pagina): ' + current.active + '<br>' +
                        'Si (resultats): ' + filter.active + '<br>' +
                        'Si (total): ' + total.active + '<hr>' +
                        'No (pagina): ' + current.inactive + '<br>' +
                        'No (resultats): ' + filter.inactive + '<br>' +
                        'No (totals): ' + total.inactive + '<hr>'
                        );

            }
        });


        // Apply the search
        table.columns().eq(0).each(function (colIdx) {
            $('input', table.column(colIdx).footer()).on('keyup change', function () {
                table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
            });
        });

    });
</script>
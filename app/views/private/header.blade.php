<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <title>Administració</title>
        {{ HTML::script('assets/jquery/jquery-2.1.3.min.js'); }}
<!--         <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js?ver=1.4.2'></script>
 -->        {{ HTML::script('assets/bs/js/bootstrap.js'); }}
        {{ HTML::style('assets/css/styles.css'); }}
        {{ HTML::style('assets/css/theme.css'); }}
        {{ HTML::style('assets/bs/css/bootstrap.css'); }}

        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

        <!-- cdn for modernizr, if you haven't included it already -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>

    </head>

    <body>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown-toggle").dropdown();
        });
    </script>

        <div class="navbar navbar-static navbar-default navbar-fixed-top navbar-collapse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle toggle-left hidden-md hidden-lg" data-toggle="sidebar" data-target=".sidebar-left">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                 <a href="{{url('/')}}"><img class="navbar-brand" src="{{url('/')}}/assets/images/mercalogo.png"></img></a>

                    <a class="navbar-brand" href="{{url('/')}}">{{$cMarket->name}}</a>

                </div>

            </div>
        </div>
        <!-- Begin page content -->
        <div class="container-fluid">

            <div class="row">


             <div class="col-xs-7 col-sm-3 col-md-2 sidebar sidebar-left sidebar-animate sidebar-md-show side-content" style="padding:0; padding-top: 20px; max-width: 230px">
                    <ul class="nav navbar-stacked">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Els meus mercats <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach (Auth::user()->markets as $key => $value)

                                <?php echo $value->name;?>
                                <li <?= $cMarket->id == $value->id ? 'class="active"' : '' ?>>
                                    <a href="{{url("move/{$value->id}")}}">
                                        {{ "<b>{$value->town->name}</b>: {$value->name}" }}
                                    </a>
                                </li>
                                @endforeach
                                <li class="divider"></li>
                                <li>{{ HTML::link('logout', 'Sortir') }}</li>
                            </ul>
                        </li>
                        @foreach ($menu as $value)
                        <li class="{{ Request::is($value['route']) ? 'active-menu' : '' }}">{{ HTML::linkRoute($value['route'], $value['text']) }}</li>
                        @endforeach
                        <li class="{{ Request::is('presence_no') ? 'active' : '' }}">{{ HTML::link('presence_no', 'Assistències') }}</li>
                        <li>{{ HTML::link('mapa', 'Mapa') }}</li>
                        <li>{{ HTML::link('logout', 'Sortir') }}</li>

                        <li><br /><br /></li>
                    </ul>
                </div> 
                <div class="main col-md-10 col-md-offset-2" style='padding: 20px;'>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Canvía la contrasenya</h2>

        <div>
            Per canviar la contrasenya accedeix al següent enllaç: {{ URL::to('password/reset', array($token)) }}.<br/>
            L'enllaç caduca en {{ Config::get('auth.reminder.expire', 60) }} minuts.
        </div>
    </body>
</html>

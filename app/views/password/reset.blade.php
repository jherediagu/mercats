

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Canviar contrasenya</title>

        {{ HTML::style('assets/bs/css/bootstrap.min.css'); }}

        <style>
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #eee;
            }

            .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin .checkbox {
                font-weight: normal;
            }
            .form-signin .form-control {
                position: relative;
                height: auto;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                padding: 10px;
                font-size: 16px;
            }
            .form-signin .form-control:focus {
                z-index: 2;
            }
            .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
            .form-signin input[type="password"] {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
        </style>
    </head>

    <body>

        <div class="container">
            <form action="{{ action('RemindersController@postReset') }}" class='form-signin' method="POST">
                <h2 class="form-signin-heading">Canviar contrasenya</h2>

                @if(Session::has('error'))
                    <div class="alert alert-danger text-center" role="alert">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        {{ Session::get('error') }}
                    </div>
                @endif
                <input type="hidden" name="token" value="{{ $token }}">
                <input type="email" placeholder="Correu" name="email" class="form-control" required>
                <input type="password" placeholder="Nova contrasenya" name="password" class="form-control" required>
                <input type="password" placeholder="Repeteix contrasenya" name="password_confirmation" class="form-control" required>
                <button type="submit" value="Reset Password" class="btn btn-lg btn-primary btn-block">Canviar</button>
            </form>
        </div> 

    </body>
</html>

